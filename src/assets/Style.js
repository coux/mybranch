import EStyleSheet from "react-native-extended-stylesheet";

export default EStyleSheet.create({
    content: {
        flex: 1,
        backgroundColor: "#fff"
    },
    center: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    centerVertical: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "center"
    },
    centerHorizontal: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center"
    },
    gradient: {
        width: "100%",
        height: "100%"
    },
    header: {
        height: "60rem",
        padding: 10
    },
    headerTitle: {
        fontSize: "18rem"
    },
    textInput: {
        flex: 1,
        fontSize: "14rem",
        height: "49rem",
        alignSelf: "stretch"
    },
    centeredText: {
        textAlign: "center"
    },
    defaultButton: {
        justifyContent: "center",
        alignItems: "center",
        height: "50rem"
    },
    roundedButton: {
        borderRadius: 5,
        overflow: "hidden"
        // justifyContent: 'center',
        // alignItems: 'center',
        // height: '50rem'
    },
    iconButton: {
        height: "50rem",
        marginVertical: 5,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: "#C3C9D4"
    },
    textButton: {
        paddingHorizontal: "20rem"
    },
    divider: {
        width: "100%",
        height: 1,
        backgroundColor: "#C3C9D4"
    },
    dividerLabel: {
        fontSize: "12rem",
        color: "#7B8088"
    },
    imageBackground: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    text11rem: {
        fontSize: "11rem"
    },
    text12rem: {
        fontSize: "12rem"
    },
    text13rem: {
        fontSize: "13rem"
    },
    text14rem: {
        fontSize: "14rem"
    },
    text15rem: {
        fontSize: "15rem"
    },
    text16rem: {
        fontSize: "16rem"
    },
    text17rem: {
        fontSize: "17rem"
    },
    text18rem: {
        fontSize: "18rem"
    },
    text19rem: {
        fontSize: "19rem"
    },
    text20rem: {
        fontSize: "20rem"
    },
    text24rem: {
        fontSize: "24rem"
    },
    textBold: {
        fontWeight: "bold"
    },
    textGray: {
        color: "#7B8088"
    },
    textRed: {
        color: "#FF3366"
    },
    textPurple: {
        color: "#7D14DE"
    },
    modalTitleWrap: {
        flex: 1,
        backgroundColor: "#fff",
        height: "49rem",
        alignSelf: "stretch",
        alignItems: "center",
        borderBottomWidth: 1,
        borderColor: "#C3C9D4",
        paddingHorizontal: 10
    },
    modalTitleText: {
        fontSize: "16rem",
        color: "#000"
    },
    gapBottom10: {
        marginBottom: 10
    },
    gapBottom20: {
        marginBottom: 20
    },
    flexWrap: {
        flexWrap: "wrap"
    },
    flexRow: {
        //flex:1,
        //alignSelf: "stretch",
        flexDirection: "row",
        alignItems: "center"
    },
    flexColumn: {
        flexDirection: "column",
        justifyContent: "center"
    },
    flexAlignStart: {
        alignItems: "flex-start",
        justifyContent: "flex-start"
    },
    flexAlignEnd: {
        alignItems: "flex-end",
        justifyContent: "flex-end"
    },
    flexBetween: {
        // flex: 1,
        alignSelf: "stretch",
        justifyContent: "space-between"
    },
    itemCenter: {
        alignItems: "center",
        justifyContent: "center"
    }
});
