import { ApolloClient, HttpLink, split } from 'apollo-boost';
import { WebSocketLink } from 'apollo-link-ws';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import { getMainDefinition } from 'apollo-utilities';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { GET_STATE } from './queries';

import Constants from 'expo-constants';
import typeDefs from './typeDefs';
import resolvers from './resolvers';

const { ip, port } =
    process.env.NODE_ENV === 'development'
        ? { ip: Constants.manifest.debuggerHost.split(':').shift(), port: 4000 }
        : { ip: '15.164.49.102', port: 80 };

const host =
    process.env.NODE_ENV === 'development' ? 'http://localhost:4000' : 'https://mybranch.co.kr';

const httpLink = new HttpLink({
    uri: `http://${ip}:${port}/graphql`
});

const client = new SubscriptionClient(`ws://${ip}:${port}/graphql`, {
    reconnect: true
});

const wsLink = new WebSocketLink(client);

const link = split(
    ({ query }) => {
        const { kind, operation } = getMainDefinition(query);
        return kind === 'OperationDefinition' && operation === 'subscription';
    },
    wsLink,
    httpLink
);

const defaultState = {
    ip,
    port,
    host,
    me: null,
    detail: null,
    routeName: 'Home',
    modal: {
        __typename: 'Modal',
        type: '',
        name: ''
    },
    __typename: 'State'
};

const cache = new InMemoryCache();

const getState = (query) => {
    return cache.readQuery({ query }).state;
};

const setState = (state) => {
    cache.writeData({ data: { state, __typename: 'State' } });
    return state;
};

setState(defaultState);

export default new ApolloClient({
    link,
    cache,
    typeDefs,
    resolvers: resolvers(getState, setState)
});
