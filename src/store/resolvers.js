import { gql } from 'apollo-boost';
import { GET_STATE } from './queries';
import { GraphQLScalarType } from 'graphql';

export default (read, write) => {
    return {
        Mutation: {
            setState: (_, { state }) => {
                const prevState = read(GET_STATE);
                const nextState = {
                    ...prevState,
                    ...state
                };
                write(nextState);
                return nextState;
            }
        }
    };
};
