import { gql } from 'apollo-boost';

// 휴대폰 인증 시간 시작
export const START_TIMER = gql`
    mutation($begin: Int!, $end: Int!, $id: String!) {
        startTimer(begin: $begin, end: $end, id: $id) {
            time
            status
        }
    }
`;

// 인증 시간 갱신
export const UPDATED_TIMER = gql`
    subscription {
        timer {
            time
            status
        }
    }
`;

// 회원 가입 요청
export const JOIN = gql`
    mutation(
        $email: String!
        $password: String!
        $nickname: String!
        $phone: String!
    ) {
        join(
            email: $email
            password: $password
            nickname: $nickname
            phone: $phone
        ) {
            id
            email
            nickname
            phone
        }
    }
`;

// 회원 로그인
export const LOGIN = gql`
    mutation($email: String!, $password: String!) {
        login(email: $email, password: $password) {
            id
            email
            nickname
            phone
        }
    }
`;

// 회원 로그인
export const LOGOUT = gql`
    mutation {
        logout
    }
`;

// 로그인 정보 요청
export const SESSION = gql`
    query {
        me {
            id
            email
            nickname
            phone
        }
    }
`;

// 문자 인증 코드 요청
export const GET_SMS_AUTH_CODE = gql`
    query($phone: String!) {
        getSmsAuthCode(phone: $phone) {
            id
            code
        }
    }
`;

// 문자 인증 코드 확인
export const VERIFY_SMS_AUTH_CODE = gql`
    query($id: String!, $code: String!) {
        verifySmsAuthCode(id: $id, code: $code) {
            verified
            message
        }
    }
`;

// 이상형 리스트 가져오기
export const GET_FAVORITES = gql`
    query {
        getFavorites {
            charm {
                index
                value
            }
        }
    }
`;
// 멀티 파일 업로드
export const MULTIPLE_UPLOAD = gql`
    mutation($files: [Upload!]!) {
        multipleUpload(files: $files) {
            images {
                flag
                large
                medium
                small
            }
        }
    }
`;
// 싱글 파일 업로드
export const SINGLE_UPLOAD = gql`
    mutation($file: Upload!) {
        singleUpload(file: $file) {
            images {
                flag
                large
                medium
                small
            }
        }
    }
`;
// 상세 정보 목록 가져오기
export const GET_USER_DETAIL_SUBJECTS = gql`
    {
        getDetailSubjects {
            genders {
                index
                value
            }
            educations {
                index
                value
            }
            jobs {
                index
                value
            }
            bloods {
                index
                value
            }
            regions {
                index
                value
            }
            bodies {
                index
                value
            }
            religions {
                index
                value
            }
            smokes {
                index
                value
            }
            drinks {
                index
                value
            }
            characters {
                index
                value
            }
            charms {
                index
                value
            }
        }
    }
`;
// 이상형 목록 가져오기
export const GET_USER_FAVOR_SUBJECTS = gql`
    {
        getFavorSubjects {
            ageStarts {
                index
                value
            }
            ageEnds {
                index
                value
            }
            regions {
                index
                value
            }
            characters {
                index
                value
            }
            heightStarts {
                index
                value
            }
            heightEnds {
                index
                value
            }
            bodies {
                index
                value
            }
            religions {
                index
                value
            }
            smokes {
                index
                value
            }
            drinks {
                index
                value
            }
            charms {
                index
                value
            }
            educations {
                index
                value
            }
        }
    }
`;
// 사용자 상세 정보 가져오기
export const GET_USER_DETAIL = gql`
    query {
        userDetail {
            gender {
                index
                value
            }
            birthdate
            education {
                index
                value
            }
            major
            job {
                index
                value
            }
            company
            blood {
                index
                value
            }
            region {
                index
                value
            }
            height
            body {
                index
                value
            }
            religion {
                index
                value
            }
            smoke {
                index
                value
            }
            drink {
                index
                value
            }
            character {
                index
                value
            }
            charm {
                index
                value
            }
            images {
                flag
                large
                medium
                small
            }
            liking
            activity
            rating {
                value
                count
            }
        }
    }
`;
// 사용자 상세 정보 설정하기
export const SET_USER_DETAIL = gql`
    mutation(
        $gender: SubjectInput!
        $birthdate: String!
        $education: SubjectInput!
        $major: String
        $job: SubjectInput!
        $company: String
        $character: [SubjectInput!]!
        $blood: SubjectInput!
        $region: SubjectInput!
        $height: String!
        $body: SubjectInput!
        $religion: SubjectInput!
        $smoke: SubjectInput!
        $drink: SubjectInput!
        $charm: [SubjectInput!]!
    ) {
        updateDetail(
            gender: $gender
            birthdate: $birthdate
            education: $education
            major: $major
            job: $job
            company: $company
            character: $character
            blood: $blood
            region: $region
            height: $height
            body: $body
            religion: $religion
            smoke: $smoke
            drink: $drink
            charm: $charm
        ) {
            gender {
                index
                value
            }
            birthdate
            education {
                index
                value
            }
            major
            job {
                index
                value
            }
            company
            character {
                index
                value
            }
            blood {
                index
                value
            }
            region {
                index
                value
            }
            height
            body {
                index
                value
            }
            religion {
                index
                value
            }
            smoke {
                index
                value
            }
            drink {
                index
                value
            }
            charm {
                index
                value
            }
        }
    }
`;
// 이상형 가져오기
export const GET_USER_FAVOR = gql`
    query {
        userFavor {
            ageStart {
                index
                value
            }
            ageEnd {
                index
                value
            }
            region {
                index
                value
            }
            character {
                index
                value
            }
            heightStart {
                index
                value
            }
            heightEnd {
                index
                value
            }
            body {
                index
                value
            }
            religion {
                index
                value
            }
            smoke {
                index
                value
            }
            drink {
                index
                value
            }
            charm {
                index
                value
            }
            education {
                index
                value
            }
        }
    }
`;
// 이상형 설정하기
export const SET_USER_FAVOR = gql`
    mutation(
        $ageStart: SubjectInput!
        $ageEnd: SubjectInput!
        $heightStart: SubjectInput!
        $heightEnd: SubjectInput!
        $education: SubjectInput!
        $body: SubjectInput!
        $religion: SubjectInput!
        $smoke: SubjectInput!
        $drink: SubjectInput!
        $charm: [SubjectInput!]!
        $character: [SubjectInput!]!
        $region: [SubjectInput!]!
    ) {
        updateFavor(
            ageStart: $ageStart
            ageEnd: $ageEnd
            heightStart: $heightStart
            heightEnd: $heightEnd
            education: $education
            body: $body
            religion: $religion
            smoke: $smoke
            drink: $drink
            charm: $charm
            character: $character
            region: $region
        ) {
            ageStart {
                index
                value
            }
            ageEnd {
                index
                value
            }
            heightStart {
                index
                value
            }
            heightEnd {
                index
                value
            }
            education {
                index
                value
            }
            body {
                index
                value
            }
            religion {
                index
                value
            }
            smoke {
                index
                value
            }
            drink {
                index
                value
            }
            charm {
                index
                value
            }
            character {
                index
                value
            }
            region {
                index
                value
            }
        }
    }
`;
// 사용자 위치 정보 가져오기
export const GET_USER_LOCATION = gql`
    query {
        userLocation {
            latitude
            longitude
        }
    }
`;
// 사용자 위치 정보 설정하기GET_USER_DETAIL
export const SET_USER_LOCATION = gql`
    mutation($latitude: Float!, $longitude: Float!) {
        updateLocation(latitude: $latitude, longitude: $longitude) {
            latitude
            longitude
        }
    }
`;
// 상점 아이템 가져오기
export const GET_STORE_ITEMS = gql`
    query {
        items {
            name
            title
            price
            help
            desc
            image
        }
    }
`;
// 사용자 알림설정 정보 가져오기
export const GET_NOTIFICATION = gql`
    query {
        userNotification {
            active
            like
            chat
            rating
            match
            location
        }
    }
`;
// 사용자 알림설정 정보 가져오기
export const SET_NOTIFICATION = gql`
    mutation(
        $active: Boolean
        $like: Boolean
        $chat: Boolean
        $rating: Boolean
        $match: Boolean
        $location: Boolean
    ) {
        updateNotification(
            active: $active
            like: $like
            chat: $chat
            rating: $rating
            match: $match
            location: $location
        ) {
            active
            like
            chat
            rating
            match
            location
        }
    }
`;
// 추천 회원 가져오기
export const GET_RECOMMANDS = gql`
    query {
        recommands {
            nickname
        }
    }
`;
// 로컬 상태 가져오기
export const GET_STATE = gql`
    query {
        state @client {
            ip
            port
            host
            routeName
            modal {
                type
                name
            }
            me {
                id
                email
                nickname
                phone
            }
            detail {
                gender {
                    index
                    value
                }
                education {
                    index
                    value
                }
                job {
                    index
                    value
                }
                blood {
                    index
                    value
                }
                region {
                    index
                    value
                }
                body {
                    index
                    value
                }
                religion {
                    index
                    value
                }
                smoke {
                    index
                    value
                }
                drink {
                    index
                    value
                }
                character {
                    index
                    value
                }
                charm {
                    index
                    value
                }
                images {
                    flag
                    large
                    medium
                    small
                }
                liking
                activity
                rating {
                    value
                    count
                }
            }
        }
    }
`;
// 로컬 상태 변경하기
export const SET_STATE = gql`
    mutation SetState($state: State) {
        setState(state: $state) @client
    }
`;
