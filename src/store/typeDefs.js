import { gql } from 'apollo-boost';

const typeDefs = gql`
    type Mutation {
        setState(state: State!): State
    }

    type State {
        ip: String!
        port: String!
        host: String
        routeName: String
        me: User
        detail: Detail
        modal: Modal
    }

    type Modal {
        type: String!
        name: String
    }
`;

export default typeDefs;
