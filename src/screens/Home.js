import React, { useState, useCallback, useLayoutEffect } from "react";
import { View, Text, Image, TouchableWithoutFeedback } from "react-native";
import { useStore } from "../components/Hooks";
import { Header } from "@react-navigation/native";
import Content from "../components/Content";
import Branch from "../assets/images/common/branch-text";
import FetchView from "../components/FetchView";
import Button from "../components/Button";
import ToggleContent from "../components/common/ToggleContent";
import Common from "../assets/Style";
import { Avatar, Badge } from "react-native-elements"; // react native elements
import faker from "faker"; // faker
import Carousel from "react-native-snap-carousel"; // carousel slider
import Swiper from "react-native-swiper"; // swiper slide

// random
const random = (range, fixed) =>
    parseInt(Math.floor(Math.random() * (range - 1) + 1).toFixed(fixed || 0));
// 추천회원 배열
// console.log(Array.from(Array(random(5)).keys()));
const recommends = Array.from(Array(10).keys()).reduce((acc) => {
    const people = {
        nickname: faker.random.word(),
        age: random(50),
		images: Array.from(Array(random(5)).keys()).map(() => {
			console.log(faker.image.avatar());
            return faker.image.avatar();
        }),
        location: random(50),
    };
    acc.push(people);
    return acc;
}, []);
console.log(recommends);

const Home = ({ navigation, data }) => {
    const { navigate } = navigation;
    const [store, updateStore] = useStore();
    const [layout, setLayout] = useState();
    const [activeIndex, setActiveIndex] = useState(0);
    // console.log('activeIndex: ', activeIndex);

    renderItem = useCallback(({ item, index }) => {
        return (
            <View
                style={{
                    backgroundColor: "#FFF",
                    borderRadius: 8,
                    height: 320,
                    marginHorizontal: 20,
                    overflow: "hidden",
                }}
            >
                <Swiper
                    showsButtons={false}
                    paginationStyle={{
                        position: "absolute",
                        top: 20,
                        left: 20,
                        height: 24,
                        justifyContent: "flex-start",
                    }}
                    dotStyle={{ width: 10, height: 10, borderRadius: 5 }}
                    dotColor={"#FFF"}
                    activeDotStyle={{ width: 10, height: 10, borderRadius: 5 }}
                    activeDotColor={"#7D14DE"}
                >
                    {item.images.map((image, i) => {
                        // console.log("image: ", image);
                        return (
                            <TouchableWithoutFeedback
                                onPress={() => {
                                    navigate("UserDetail", {
                                        userData: item,
                                        width: layout.width,
                                    });
                                }}
                                key={`profile${i}`}
                            >
                                <View>
                                    <Image
                                        source={{ url: image }}
                                        style={{ width: "100%", height: "100%" }}
                                    />
                                </View>
                            </TouchableWithoutFeedback>
                        );
                    })}
                </Swiper>

                {/* 매니저 뱃지, 번개 아이템 */}
                <View style={[Common.flexRow, { position: "absolute", top: 20, right: 20 }]}>
                    <Badge
                        value="I"
                        badgeStyle={{
                            width: 24,
                            height: 24,
                            backgroundColor: "#DDD",
                            borderRadius: 50,
                            marginRight: 10,
                        }}
                        textStyle={{
                            fontSize: 14,
                            color: "#FFF",
                        }}
                    />
                    <Badge
                        value="M"
                        badgeStyle={{
                            width: 24,
                            height: 24,
                            backgroundColor: "#7D14DE",
                            borderRadius: 50,
                            marginRight: 10,
                        }}
                        textStyle={{
                            fontSize: 14,
                            color: "#FFF",
                        }}
                    />
                </View>
                {/* 이름, 나이, 거리 */}
                <View
                    style={[
                        Common.flexRow,
                        Common.flexBetween,
                        Common.flexWrap,
                        {
                            flex: 1,
                            width: "100%",
                            position: "absolute",
                            zIndex: 10,
                            bottom: 0,
                            left: 0,
                            padding: 20,
                        },
                    ]}
                >
                    <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 24, color: "#FFF" }}>
                            {item.nickname} ({item.age})
                        </Text>
                    </View>
                    <View
                        style={[
                            {
                                flex: 1,
                                alignSelf: "flex-end",
                                alignItems: "flex-end",
                                justifyContent: "flex-end",
                            },
                        ]}
                    >
                        <Text style={{ fontSize: 14, color: "#FFF" }}>{item.location}km</Text>
                    </View>
                </View>
            </View>
        );
    });

    return (
        <View style={{ flex: 1 }} onLayout={({ nativeEvent: { layout } }) => setLayout(layout)}>
            <View
                style={{
                    flex: 1,
                    flexDirection: "row",
                    justifyContent: "center",
                }}
            >
                <Carousel
                    layout={"default"}
                    inactiveSlideOpacity={0.2}
                    inactiveSlideScale={0.9}
                    vertical={true}
                    data={recommends}
                    sliderWidth={320}
                    sliderHeight={layout ? layout.height : 320}
                    itemWidth={320}
                    itemHeight={320}
                    renderItem={renderItem}
                    // onSnapToItem={(index) => setActiveIndex(index)}
                />
            </View>
        </View>
    );
};

export default Home;
