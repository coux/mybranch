import React, {
    useState,
    useCallback,
    useEffect,
    useLayoutEffect,
} from 'react';
import { View, Text } from 'react-native';
// import { NavigationEvents } from '@react-navigation/stack';
import { useUpload } from '../components/Hooks';
import { graphql } from '@apollo/react-hoc';
import { GET_USER_DETAIL } from '../store/queries';
import Content from '../components/Content';
import Button from '../components/Button';
import PhotoView from '../components/profile/PhotoView';

const labels = ['대표', '필수', '선택', '선택', '선택', '선택'];
const MyPhoto = (props) => {
    const {
        navigation,
        data: { userDetail },
    } = props;

    // const [store, updateStore] = useStore();
    const [result, upload] = useUpload({ multiple: true });
    const [photos, setPhotos] = useState([]);

    const onAdded = useCallback((photo) => {
        photo = { ...photo, flag: photos.length };
        setPhotos([...photos, photo]);
    }, []);

    const onRemove = () => {};
    useEffect(() => {
        if (userDetail) {
            let { images } = userDetail;
            if (images) {
                images = images.map((image) => ({
                    ...image,
                    uri: image.large,
                }));
                setPhotos(images);
            }
        }
    }, [userDetail]);

    useEffect(() => {
        if (result) {
            navigation.navigate('MyDetail');
        }
    }, [result]);

    useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <Button
                    style={{ padding: 10, marginRight: 10 }}
                    onPress={() => {
                        upload(photos);
                    }}
                >
                    <Text style={{ fontSize: 18, color: '#7D14DE' }}>완료</Text>
                </Button>
            ),
        });
    }, [photos, navigation]);

    return (
        <Content>
            <Text style={{ paddingVertical: 10 }}>
                반드시 본인의 사진을 올려주세요
            </Text>
            <View
                style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    flexWrap: 'wrap',
                }}
            >
                {labels.map((label, i) => {
                    return (
                        <PhotoView
                            index={i}
                            photo={photos[i]}
                            key={`photo${i}`}
                            label={label}
                            containerStyle={{ width: '31.5%', marginBottom: 8 }}
                            onAdded={onAdded}
                            onRemoved={onRemove}
                        />
                    );
                })}
            </View>
        </Content>
    );
};
export default graphql(GET_USER_DETAIL)(MyPhoto);
