import React, { useState } from 'react';
import { View, Text } from 'react-native';
import Header from '../components/Header';
import Button from '../components/Button';
import Content from '../components/Content';
import Common from '../assets/Style';
import ArrowLeft from '../assets/images/common/arrow-left';
import Modal from 'react-native-modal';
import { TouchableOpacity } from 'react-native-gesture-handler';

const Test = props => {
    const [modal1, setModa1] = useState(false);
    const [modal2, setModa2] = useState(false);
    return (
        <Content>
            <TouchableOpacity onPress={() => setModa1(true)}>
                <Text>모달1</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => setModa2(true)}>
                <Text>모달2</Text>
            </TouchableOpacity>
            <Modal
                isVisible={modal1}
                animationIn="zoomInDown"
                animationOut="zoomOutUp"
                animationInTiming={1000}
                animationOutTiming={1000}
                backdropTransitionInTiming={800}
                backdropTransitionOutTiming={800}
            >
                <View style={{ flex: 1, backgroundColor: 'white' }}>
                    <Text>모달1</Text>
                </View>
            </Modal>
            <Modal
                style={{ marginLeft: 0 }}
                isVisible={modal2}
                animationIn="slideInDown"
                animationOut="slideOutUp"
                animationInTiming={1000}
                animationOutTiming={1000}
                backdropTransitionInTiming={800}
                backdropTransitionOutTiming={800}
            >
                <View style={{ flex: 1, backgroundColor: 'white' }}>
                    <Text>모달2</Text>
                </View>
            </Modal>
        </Content>
    );
};

Test.navigationOptions = ({ navigation }) => ({
    header: (
        <Header title="회원가입" titleStyle={Common.headerTitle} containerStyle={Common.header}>
            <Button
                onPress={() => {
                    navigation.navigate('Login');
                }}
            >
                <ArrowLeft />
            </Button>
        </Header>
    )
});

export default Test;
