import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, Dimensions } from 'react-native';
import { CheckBox } from 'react-native-elements';
import InputScrollView from 'react-native-input-scroll-view';
import EStyleSheet from 'react-native-extended-stylesheet';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { graphql } from '@apollo/react-hoc';
import { useStore, useModal, useDeleteProps } from '../components/Hooks';
import { GET_USER_DETAIL, GET_USER_DETAIL_SUBJECTS, SET_USER_DETAIL } from '../store/queries';
import FetchView from '../components/FetchView';
import MenuPicker from '../components/common/MenuPicker';
import DropDown from '../components/common/DropDown';
import Button from '../components/Button';
import Content from '../components/Content';
import ToggleContent from '../components/common/ToggleContent';
import Common from '../assets/Style';
import TextInput from '../components/InputText';

const device = Dimensions.get('window');

const MyDetail = ({ navigation, data }) => {
    const { loading, error, getDetailSubjects: subjects } = data;
    if (loading || error) return <FetchView loading={loading} error={error} />;
    const { navigate } = navigation;
    const [modalType, setModalType] = useState('');
    const [store, updateStore] = useStore();
    const [getDetail, { data: getDetailRes }] = useLazyQuery(GET_USER_DETAIL);
    const [setDetail, { data: setDetailRes }] = useMutation(SET_USER_DETAIL);
    const { me, detail } = store;

    const results = setDetailRes || getDetailRes || {};
    // 성별(필수)
    const [gender, setGender] = useState(1);
    // 생년월일(필수)
    const [birthdate, setBirthdate] = useState('20200101');
    // 최종학력(필수)
    const [education, setEducation] = useState(1);
    // 전공(선택)
    const [major, setMajor] = useState('법학');
    // 직업(필수)
    const [job, setJob] = useState(5);
    // 직장명(선택)
    const [company, setCompany] = useState('서울대검찰청');
    // 혈액형(필수)
    const [blood, setBlood] = useState(5);
    // 지역(필수)
    const [region, setRegion] = useState(1);
    // 키(필수)
    const [height, setHeight] = useState('185.5');
    // 체형(필수)
    const [body, setBody] = useState(6);
    // 종교(필수)
    const [religion, setReligion] = useState(2);
    // 흡연(필수)
    const [smoke, setSmoke] = useState(2);
    // 음주(필수)
    const [drink, setDrink] = useState(3);
    // 성격(필수)
    const [character, setCharacter] = useState([]);
    // 매력(필수)
    const [charm, setCharm] = useState([]);
    // const open = useModal();

    useEffect(() => {
        if (me) {
            getDetail();
        }
    }, [me]);

    useEffect(() => {
        const { userDetail } = results;
        if (userDetail) {
            setGender(userDetail.gender.index);
            setBirthdate(userDetail.birthdate);
            setEducation(userDetail.education.index);
            setMajor(userDetail.major);
            setJob(userDetail.job.index);
            setCompany(userDetail.company);
            setBlood(userDetail.blood.index);
            setRegion(userDetail.region.index);
            setHeight(userDetail.height);
            setBody(userDetail.body.index);
            setReligion(userDetail.religion.index);
            setSmoke(userDetail.smoke.index);
            setDrink(userDetail.drink.index);
            setCharacter(userDetail.character);
            setCharm(userDetail.charm);
        }
    }, [results]);

    useEffect(() => {
        if (setDetailRes) {
            navigate('MyFavor');
        }
    }, [setDetailRes]);

    return (
        <InputScrollView keyboardShouldPersistTaps="always">
            <Content>
                <View style={{ marginBottom: 20 }}>
                    <Button
                        style={{
                            ...Common.defaultButton,
                            ...Common.roundedButton,
                            borderWidth: 1,
                            borderColor: '#7D14DE',
                            backgroundColor: '#7D14DE'
                        }}
                        onPress={() => navigate('MyPhoto')}
                    >
                        <Text style={{ color: '#FFFFFF' }}>사진등록</Text>
                    </Button>
                </View>

                <DropDown
                    title="성별"
                    value={subjects.genders[gender]}
                    placeholder="성별을 선택하세요"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '성별 선택',
                        containerStyle: styles.dropMenu
                    }}
                >
                    <MenuPicker
                        pickerStyle={styles.picker}
                        pickerItemStyle={styles.pickerItem}
                        selected={gender}
                        onSelect={setGender}
                        items={subjects.genders}
                    />
                </DropDown>

                <TextInput
                    containerStyle={{ marginBottom: 20 }}
                    style={Common.textInput}
                    label="생년월일"
                    placeholder="예)"
                    value={birthdate}
                    clearButton={true}
                    autoCapitalize="none"
                    keyboardType="numeric"
                    onChangeText={(value) => setBirthdate(value)}
                />

                <DropDown
                    title="최종학력"
                    value={subjects.educations[education]}
                    placeholder="최종학력을 선택하세요"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '최종학력 선택',
                        containerStyle: styles.dropMenu
                    }}
                >
                    <MenuPicker
                        pickerStyle={styles.picker}
                        pickerItemStyle={styles.pickerItem}
                        selected={education}
                        onSelect={setEducation}
                        items={subjects.educations}
                    />
                </DropDown>

                <TextInput
                    containerStyle={{ marginBottom: 20 }}
                    style={Common.textInput}
                    label="전공 (선택)"
                    placeholder="전공을 입력하세요"
                    value={major}
                    clearButton={true}
                    autoCapitalize="none"
                    onChangeText={(value) => setMajor(value)}
                />

                <DropDown
                    title="직업"
                    value={subjects.jobs[job]}
                    placeholder="직업을 선택하세요"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '직업 선택',
                        containerStyle: styles.dropMenu
                    }}
                >
                    <MenuPicker
                        pickerStyle={styles.picker}
                        pickerItemStyle={styles.pickerItem}
                        selected={job}
                        onSelect={setJob}
                        items={subjects.jobs}
                    />
                </DropDown>

                <TextInput
                    containerStyle={{ marginBottom: 20 }}
                    style={Common.textInput}
                    label="직장명"
                    placeholder="직장명을 입력하세요"
                    value={company}
                    clearButton={true}
                    autoCapitalize="none"
                    onChangeText={(value) => setCompany(value)}
                />
                <DropDown
                    title="혈액형"
                    value={subjects.bloods[blood]}
                    placeholder="혈액형을 선택하세요"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '혈액형 선택',
                        containerStyle: styles.dropMenu
                    }}
                >
                    <MenuPicker
                        pickerStyle={styles.picker}
                        pickerItemStyle={styles.pickerItem}
                        selected={blood}
                        onSelect={setBlood}
                        items={subjects.bloods}
                    />
                </DropDown>

                <DropDown
                    title="지역"
                    value={subjects.regions[region]}
                    placeholder="지역을 선택하세요"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '지역 선택',
                        containerStyle: styles.dropMenu
                    }}
                >
                    <MenuPicker
                        pickerStyle={styles.picker}
                        pickerItemStyle={styles.pickerItem}
                        selected={region}
                        onSelect={setRegion}
                        items={subjects.regions}
                    />
                </DropDown>

                <TextInput
                    containerStyle={{ marginBottom: 20 }}
                    style={Common.textInput}
                    label="키"
                    placeholder="자신의 키를 입력하세요"
                    value={height.toString()}
                    clearButton={true}
                    autoCapitalize="none"
                    onChangeText={(value) => setHeight(value)}
                />
                <DropDown
                    title="체형"
                    value={subjects.bodies[body]}
                    placeholder="체형을 선택하세요"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '체형 선택',
                        containerStyle: styles.dropMenu
                    }}
                >
                    <MenuPicker
                        pickerStyle={styles.picker}
                        pickerItemStyle={styles.pickerItem}
                        selected={body}
                        onSelect={setBody}
                        items={subjects.bodies}
                    />
                </DropDown>
                <DropDown
                    title="종교"
                    value={subjects.religions[religion]}
                    placeholder="종교를 선택하세요"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '종교 선택',
                        containerStyle: styles.dropMenu
                    }}
                >
                    <MenuPicker
                        pickerStyle={styles.picker}
                        pickerItemStyle={styles.pickerItem}
                        selected={religion}
                        onSelect={setReligion}
                        items={subjects.religions}
                    />
                </DropDown>
                <DropDown
                    title="흡연"
                    value={subjects.smokes[smoke]}
                    placeholder="흡연 여부를 선택하세요"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '혈액형 선택',
                        containerStyle: styles.dropMenu
                    }}
                >
                    <MenuPicker
                        pickerStyle={styles.picker}
                        pickerItemStyle={styles.pickerItem}
                        selected={smoke}
                        onSelect={setSmoke}
                        items={subjects.smokes}
                    />
                </DropDown>
                <DropDown
                    title="음주"
                    value={subjects.drinks[drink]}
                    placeholder="음주 여부를 선택하세요"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '음주량 선택',
                        containerStyle: styles.dropMenu
                    }}
                >
                    <MenuPicker
                        pickerStyle={styles.picker}
                        pickerItemStyle={styles.pickerItem}
                        selected={drink}
                        onSelect={setDrink}
                        items={subjects.drinks}
                    />
                </DropDown>

                {/* 성격 선택 */}
                <View style={{ marginBottom: 20 }}>
                    <ToggleContent
                        title="성격 (최대 6개 선택)"
                        titleStyle={styles.title}
                        items={subjects.characters}
                        itemStyle={styles.item}
                        containerStyle={styles.toggle}
                        selecteds={character}
                        onChange={setCharacter}
                        limit={6}
                    />
                </View>

                <View style={{ marginBottom: 20 }}>
                    <ToggleContent
                        title="내 매력을 선택해주세요."
                        titleStyle={styles.title}
                        items={subjects.charms}
                        itemStyle={styles.item}
                        containerStyle={styles.toggle}
                        selecteds={charm}
                        onChange={setCharm}
                    />
                </View>
            </Content>
            <View>
                <Button
                    style={{
                        ...Common.defaultButton,
                        ...Common.roundedButton,
                        borderRadius: 0,
                        borderWidth: 1,
                        borderColor: '#7D14DE',
                        backgroundColor: '#7D14DE'
                    }}
                    onPress={() => {
                        setDetail({
                            variables: useDeleteProps(
                                {
                                    birthdate,
                                    major,
                                    company,
                                    height,
                                    gender: subjects.genders[gender],
                                    education: subjects.educations[education],
                                    job: subjects.jobs[job],
                                    blood: subjects.bloods[blood],
                                    region: subjects.regions[region],
                                    body: subjects.bodies[body],
                                    religion: subjects.religions[religion],
                                    smoke: subjects.smokes[smoke],
                                    drink: subjects.drinks[drink],
                                    charm,
                                    character
                                },
                                '__typename'
                            )
                        });
                    }}
                >
                    <Text style={{ color: '#FFFFFF' }}>완료</Text>
                </Button>
            </View>
        </InputScrollView>
    );
};

export default graphql(GET_USER_DETAIL_SUBJECTS)(MyDetail);

const styles = EStyleSheet.create({
    title: {
        height: '15rem',
        marginBottom: 5,
        fontSize: 12,
        color: '#2A2E34'
    },
    item: {
        padding: 10,
        borderRadius: 4,
        borderWidth: 1,
        marginRight: 4,
        marginBottom: 4
    },
    toggle: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 4
    },
    dropMenu: {
        marginTop: -3,
        borderWidth: 1,
        borderColor: '#C3C9D4'
    },
    picker: {
        height: 200,
        backgroundColor: '#fff',
        overflow: 'hidden'
    },
    pickerItem: {
        color: '#000',
        fontSize: 20,
        marginTop: -10,
        borderWidth: 0
    }
});
