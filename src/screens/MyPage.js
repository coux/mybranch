import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, Dimensions } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { GET_USER_DETAIL } from '../store/queries';
import { graphql } from '@apollo/react-hoc';
import { useStore } from '../components/Hooks';
import FetchView from '../components/FetchView';
import Button from '../components/Button';
import Content from '../components/Content';
import ToggleContent from '../components/common/ToggleContent';
import Common from '../assets/Style';

import { Avatar, Badge } from 'react-native-elements'; // react native elements
import IcoGroup from '../assets/images/common/ico_group_setting'; // 그룹아이콘
import ArrowRight from '../assets/images/common/arrow-right-gray';

const MyPage = ({ navigation }) => {
    const { navigate, replace } = navigation;
    const [detail, setDetail] = useState();
    const [store] = useStore();
    const { me } = store;
    const [getDetail, { data, loading, error }] = useLazyQuery(GET_USER_DETAIL);
    console.log('data: ', data);

    useEffect(() => {
        if (me) {
            getDetail();
        }
    }, [me]);

    useEffect(() => {
        if (!loading && data) {
            const { userDetail } = data;
            if (userDetail) {
                setDetail(userDetail);
            } else {
                replace('MyDetail');
            }
        }
    }, [data, loading]);
    console.log(detail);
    if (loading || error || !detail) return <FetchView loading={loading} error={error} />;

    return (
        <View>
            <View style={[styles.container]}>
                <View
                    style={[Common.flexRow, Common.flexBetween, Common.gapBottom10, { height: 24 }]}
                >
                    <View style={[Common.flexRow]}>
                        <Text style={[Common.text12rem, Common.textGray]}>받은좋아요 : </Text>
                        <Text style={[Common.text12rem]}>{detail.liking}</Text>
                    </View>
                    <Button>
                        <IcoGroup width={24} heightf={24} />
                    </Button>
                </View>
                {/* 프로필 사진 및 기본 평점 row */}
                <View style={[Common.flexRow, Common.flexBetween, Common.gapBottom20]}>
                    <View>
                        <Avatar
                            size={96}
                            containerStyle={{
                                borderRadius: 8,
                                overflow: 'hidden'
                            }}
                            source={{
                                uri: detail.images[0].large
                            }}
                        />
                    </View>
                    <View style={[Common.flexBetween, { flex: 1, paddingLeft: 20 }]}>
                        {/* 닉네임, 등급 row */}
                        <View style={[Common.flexRow, Common.flexBetween]}>
                            <View style={[Common.flexRow]}>
                                {/* M bedge는 매니저(그룹장) 일때만 출력 */}
                                <Badge
                                    value="M"
                                    badgeStyle={{
                                        width: 24,
                                        height: 24,
                                        backgroundColor: '#7D14DE',
                                        borderRadius: 50,
                                        marginRight: 10
                                    }}
                                    textStyle={{
                                        fontSize: 14,
                                        color: '#FFF'
                                    }}
                                />
                                <Text>{me && me.nickname}</Text>
                            </View>
                            <View style={[styles.roundButton, Common.itemCenter]}>
                                <Text style={[Common.text11rem]}>S등급</Text>
                            </View>
                        </View>
                        {/* 평점 row */}
                        <View style={[Common.flexRow, Common.flexBetween]}>
                            <View style={[Common.itemCenter, { width: 60 }]}>
                                <Text
                                    style={[Common.text12rem, Common.textGray, Common.gapBottom10]}
                                >
                                    인기지수
                                </Text>
                                <Text style={[Common.text24rem, Common.textGray, Common.textBold]}>
                                    {detail.rating.value}
                                </Text>
                            </View>
                            <View style={[Common.itemCenter, { width: 60 }]}>
                                <Text
                                    style={[Common.text12rem, Common.textGray, Common.gapBottom10]}
                                >
                                    활동지수
                                </Text>
                                <Text style={[Common.text16rem, Common.textGray, Common.textBold]}>
                                    {detail.activity}
                                </Text>
                            </View>
                            <View style={[Common.itemCenter, { width: 60 }]}>
                                <Text
                                    style={[Common.text12rem, Common.textGray, Common.gapBottom10]}
                                >
                                    전체평점
                                </Text>
                                <Text
                                    style={[Common.text24rem, Common.textPurple, Common.textBold]}
                                >
                                    4.56
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
                {/* 트리, 쪽지, 채팅 */}
                <View style={[Common.flexRow]}>
                    <View style={[Common.itemCenter, { width: '33.3333%' }]}>
                        <Text style={[Common.text14rem]}>내 트리</Text>
                        <Text style={[Common.text13rem]}>2,000k</Text>
                    </View>
                    <View style={[Common.itemCenter, { width: '33.3333%' }]}>
                        <Text style={[Common.text14rem]}>쪽지</Text>
                        <Text style={[Common.text13rem]}>168</Text>
                    </View>
                    <View style={[Common.itemCenter, { width: '33.3333%' }]}>
                        <Text style={[Common.text14rem]}>채팅</Text>
                        <Text style={[Common.text13rem]}>16</Text>
                    </View>
                </View>
            </View>
            {/* 프로필 수정, 이상형 설정 button */}
            <View style={[Common.flexRow, Common.flexBetween, Common.gapBottom20]}>
                <Button
                    style={[
                        Common.itemCenter,
                        {
                            height: 50,
                            flex: 1,
                            backgroundColor: '#F0F3F8',
                            borderWidth: 1,
                            borderColor: '#D8E1F0'
                        }
                    ]}
                    onPress={() => navigate('UserProfile')}
                    onPress={() => navigate('MyDetail')}
                >
                    <Text style={[Common.text14rem]}>프로필수정</Text>
                </Button>
                <Button
                    style={[
                        Common.itemCenter,
                        {
                            height: 50,
                            flex: 1,
                            backgroundColor: '#F0F3F8',
                            borderWidth: 1,
                            borderColor: '#D8E1F0'
                        }
                    ]}
                    onPress={() => navigate('MyFavor')}
                >
                    <Text style={[Common.text14rem]}>이상형설정</Text>
                </Button>
            </View>
            <View>
                <Button
                    style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}
                >
                    <Text style={[Common.text14rem]}>트리 충전하기</Text>
                    <ArrowRight width={24} height={24} />
                </Button>
                <Button
                    style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}
                >
                    <Text style={[Common.text14rem]}>그룹원 초대하기</Text>
                    <ArrowRight width={24} height={24} />
                </Button>
                <Button
                    style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}
                >
                    <Text style={[Common.text14rem]}>고객센터</Text>
                    <ArrowRight width={24} height={24} />
                </Button>
                <Button
                    style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}
                    onPress={() => navigate('Settings')}
                >
                    <Text style={[Common.text14rem]}>설정</Text>
                    <ArrowRight width={24} height={24} />
                </Button>
            </View>
        </View>
    );
};

export default MyPage;

const styles = EStyleSheet.create({
    container: {
        padding: 20
    },
    roundButton: {
        width: 48,
        height: 24,
        backgroundColor: '#F0F3F8',
        borderRadius: 50
    }
});
