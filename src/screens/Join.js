import React, { useState, useEffect } from 'react';
import { View, Keyboard } from 'react-native';
import { GET_STATE } from '../store/queries';
import { useStore } from '../components/Hooks';
import ArrowLeft from '../assets/images/common/arrow-left';
import Content from '../components/Content';

import Button from '../components/Button';
import ButtonAuth from '../components/ButtonAuth';
import InputAuth from '../components/InputAuth';
import ButtonSign from '../components/ButtonSign';
import ButtonVerify from '../components/ButtonVerify';
import Common from '../assets/Style';
import TextInput from '../components/InputText';
import InputScrollView from 'react-native-input-scroll-view';

const Join = ({ data, navigation }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [verifyPassword, setVerifyPassword] = useState('');
    const [nickname, setNickname] = useState('');
    const [phone, setPhone] = useState('');
    const [authCode, setAuthCode] = useState('');
    const [authState, setAuthState] = useState('pending');
    const [validateId, setValidateId] = useState('');
    const [store, updateStore] = useStore();
    const { navigate } = navigation;
    const { me } = store;
    useEffect(() => {
        if (me) navigate('Home');
    }, [me]);

    return (
        <InputScrollView>
            <Content>
                <TextInput
                    containerStyle={{ marginBottom: 20 }}
                    style={Common.textInput}
                    label="이메일"
                    placeholder="이메일을 입력하세요"
                    value={email}
                    autoCapitalize="none"
                    keyboardType="email-address"
                    clearButton={true}
                    onChangeText={setEmail}
                />
                <TextInput
                    containerStyle={{ marginBottom: 20 }}
                    style={Common.textInput}
                    label="비밀번호"
                    placeholder="비밀번호를 입력하세요"
                    value={password}
                    clearButton={true}
                    secureButton={true}
                    secureTextEntry={true}
                    blurOnSubmit={false}
                    onSubmitEditing={Keyboard.dismiss}
                    onChangeText={setPassword}
                />
                <TextInput
                    containerStyle={{ marginBottom: 20 }}
                    style={Common.textInput}
                    label="비밀번호 확인"
                    placeholder="비밀번호를 한번 더 입력하세요"
                    value={verifyPassword}
                    clearButton={true}
                    secureButton={true}
                    secureTextEntry={true}
                    blurOnSubmit={false}
                    onSubmitEditing={Keyboard.dismiss}
                    onChangeText={setVerifyPassword}
                />
                <TextInput
                    containerStyle={{ marginBottom: 20 }}
                    style={Common.textInput}
                    label="닉네임"
                    placeholder="닉네임을 입력하세요"
                    value={nickname}
                    clearButton={true}
                    autoCapitalize="none"
                    onChangeText={setNickname}
                />
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'flex-end',
                        marginBottom: 20
                    }}
                >
                    <TextInput
                        containerStyle={{ flex: 1, marginRight: 5 }}
                        style={{ ...Common.textInput }}
                        label="핸드폰 번호"
                        placeholder={`"-" 없이 입력 하세요`}
                        value={phone}
                        clearButton={true}
                        autoCapitalize="none"
                        keyboardType="phone-pad"
                        editable={authState === 'pending'}
                        onChangeText={setPhone}
                    />

                    {phone.length > 0 && (
                        <ButtonAuth
                            authState={authState}
                            setAuthState={setAuthState}
                            setAuthCode={setAuthCode}
                        />
                    )}
                </View>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'flex-end',
                        marginBottom: 20
                    }}
                >
                    {authState !== 'pending' && (
                        <InputAuth
                            phone={phone}
                            setAuthCode={setAuthCode}
                            setAuthState={setAuthState}
                            setValidateId={setValidateId}
                        />
                    )}
                    {authCode.length > 0 && (
                        <ButtonVerify
                            validateId={validateId}
                            authCode={authCode}
                            setAuthState={setAuthState}
                        />
                    )}
                </View>
            </Content>
            <ButtonSign
                navigate={navigation.navigate}
                authState={authState}
                email={email}
                password={password}
                nickname={nickname}
                phone={phone}
            />
        </InputScrollView>
    );
};

export default Join;
