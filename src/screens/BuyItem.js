import React, { useEffect } from 'react';
import { View, Image, Text } from 'react-native';
import { useMutation } from '@apollo/react-hooks';
import { useStore } from '../components/Hooks';
import Button from '../components/Button';
import Content from '../components/Content';
import Common from '../assets/Style';
import EStyleSheet from 'react-native-extended-stylesheet';

const BuyItem = (props) => {
    const { navigate } = props.navigation;
    const [store, updateStore] = useStore();
    const { data } = props;
    let { price, name, image, desc } = props.route.params

    return (
      <View>
        <View style={styles.wrapperBox}>
          <View style={[styles.wrapper, Common.flexRow]}>
            <View style={styles.iconImgWrap}>
              <Image 
                source={image}
                style={styles.iconImg}
              />
            </View>
            <View style={styles.itemTextWrap}>
              <Text style={styles.itemNameText}>
                {name}
              </Text>
              <Text style={styles.priceText}>
                {price} 원
              </Text>
            </View>
          </View>
        </View>

        <View style={styles.itemTextWrap}>
          <Text style={styles.titleText}>
            {name}이란?
          </Text>
          <Text style={styles.itemDescText}>
            {desc}
          </Text>
          <View style={[Common.divider]}></View>
          <Text style={styles.itemDetailText}>
            아이템의 이용과 결제 내역은 마이페이지에서 가능합니다. 결제한 아이템은 마이페이지 > 내아이템에서 적용 됩니다. 결제한 아이템은 미사용한 아이템의 경우 선물이 가능합니다. 미성년자 가입자는 팝콘 및 유료아이템을 구매할 수 없습니다. 법정대리인의 동의 없이 미성년자명의로 결제한 가능합니다.
          </Text>
        </View>
        <Button style={styles.btnBuyItem}>
          <Text style={styles.itemBuyText}>구매하기</Text>
        </Button>
      </View>
    );
};

export default BuyItem;

const styles = EStyleSheet.create({
  wrapperBox:{
    backgroundColor: '#F0F3F8'
  },
  wrapper: {
    margin: 20,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#D8E1F0',
    backgroundColor: 'white'
  },
  iconImgWrap: {
    padding: 24,
    borderRightWidth: 1,
    borderColor: '#D8E1F0',
  },
  iconImg: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
  },
  
  itemTextWrap: {
    padding: 20
  },
  itemDescText: {
    marginBottom: 20,
    fontSize: 14,
    fontWeight: 'bold',
    lineHeight: 20,
    color: '#2A2E34'
  },
  itemDetailText: {
    marginTop: 20,
    fontSize: 12,
  },
  titleText: {
    fontSize: 18,
    fontWeight: 'bold',
    lineHeight: 26,
    color: '#7D14DE'
  },
  itemNameText: {
    fontWeight: 'bold'
  },
  priceText: {
    fontSize: 18,
    fontWeight: 'bold',
    lineHeight: 26,
    color: '#7D14DE'
  },
  btnBuyItem: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    color: 'white',
    backgroundColor: '#7D14DE'
  },
  itemBuyText: {
    width: '100%',
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white'
  },
});
