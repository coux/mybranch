import React, { useState, useEffect } from "react";
import { View, Image, Text, StyleSheet, ScrollView } from "react-native";
import { useMutation } from "@apollo/react-hooks";
import { useStore } from "../components/Hooks";
import Content from "../components/Content";
import Button from "../components/Button";
import Common from "../assets/Style"; // common style
import { Rating } from "react-native-elements"; // react native elements rating
import Swiper from "react-native-swiper"; // swiper slide

// svg
import IcoItem from "../assets/images/common/ic_speed_ting";
import IcoLocation from "../assets/images/common/ic_location_gray";
import IcoClose from "../assets/images/common/btn_close_white";
import IcoMsg from "../assets/images/common/ico_msg_w";
import IcoLightning from "../assets/images/common/ico_lightning_w";
import IcoLike from "../assets/images/common/ico_like_w";
// gradient
import { LinearGradient } from "expo-linear-gradient";

const random = (range, fixed) => Math.floor(Math.random() * range).toFixed(fixed || 0); // random
const charms = [
    { index: 1, value: "또렷한 이목구비" },
    { index: 2, value: "핫바디" },
    { index: 3, value: "쌍커플" },
    { index: 4, value: "오똑한 콧날" },
    { index: 5, value: "근육질 몸" },
    { index: 6, value: "금사빠" },
    { index: 7, value: "얼굴천재" },
    { index: 8, value: "풍부한 지식" },
    { index: 9, value: "차도남" },
    { index: 10, value: "착한 성품" },
    { index: 11, value: "분위기 메이커" },
    { index: 12, value: "의리남/녀" },
    { index: 13, value: "개성있는 얼굴" },
    { index: 14, value: "간지남/녀" },
    { index: 15, value: "직진형" },
    { index: 16, value: "리더십" },
    { index: 17, value: "글래머" },
    { index: 18, value: "근육질" },
    { index: 19, value: "핵인싸" },
];
const characters = [
    { index: 1, value: "쾌활한" },
    { index: 2, value: "내성적인" },
    { index: 3, value: "유머있는" },
    { index: 4, value: "차분한" },
    { index: 5, value: "낙천적인" },
    { index: 6, value: "감각적인" },
    { index: 7, value: "감성적인" },
    { index: 8, value: "단순" },
    { index: 9, value: "열정적인" },
    { index: 10, value: "개성있는" },
    { index: 11, value: "듬직한" },
    { index: 12, value: "귀여운" },
];
const bloods = [
    { index: 1, value: "A형" },
    { index: 2, value: "B형" },
    { index: 3, value: "O형" },
    { index: 4, value: "AB형" },
    { index: 5, value: "기타" },
];
const genders = [
    { index: 1, value: "남" },
    { index: 2, value: "여" },
];
const educations = [
    { index: 1, value: "고등학교" },
    { index: 2, value: "전문대" },
    { index: 3, value: "대학교" },
    { index: 4, value: "대학원(석사)" },
    { index: 5, value: "대학원(박사)" },
    { index: 6, value: "기타" },
];
const regions = [
    { index: 1, value: "서울" },
    { index: 2, value: "부산" },
    { index: 3, value: "대구" },
    { index: 4, value: "인천" },
    { index: 5, value: "광주" },
    { index: 6, value: "대전" },
    { index: 7, value: "울산" },
    { index: 8, value: "세종" },
    { index: 9, value: "경기" },
    { index: 10, value: "강원" },
    { index: 11, value: "충북" },
    { index: 12, value: "충남" },
    { index: 13, value: "전북" },
    { index: 14, value: "전남" },
    { index: 15, value: "경북" },
    { index: 16, value: "경남" },
    { index: 17, value: "제주" },
    { index: 18, value: "해외" },
];
const bodies = [
    { index: 1, value: "스키니" },
    { index: 2, value: "조금마름" },
    { index: 3, value: "보통" },
    { index: 4, value: "슬림하며 단단" },
    { index: 5, value: "근육질" },
    { index: 6, value: "조금 통통" },
    { index: 7, value: "통통" },
];
const religions = [
    { index: 1, value: "무교" },
    { index: 2, value: "기독교" },
    { index: 3, value: "천주교" },
    { index: 4, value: "불교" },
    { index: 5, value: "원불교" },
    { index: 6, value: "유교" },
    { index: 7, value: "도교" },
    { index: 8, value: "이슬람교" },
    { index: 9, value: "힌두교" },
    { index: 10, value: "유대교" },
    { index: 11, value: "기타" },
];
const drinks = [
    { index: 1, value: "전혀 마시지 않아요" },
    { index: 2, value: "어쩔 수 없을 때만 마셔요" },
    { index: 3, value: "가끔 마셔요" },
    { index: 4, value: "어느 정도 즐기는 편이에요" },
    { index: 5, value: "자주 술자리를 가져요" },
];
const jobs = [
    { index: 1, value: "경영·사무" },
    { index: 2, value: "미디어" },
    { index: 3, value: "전문·특수직" },
    { index: 4, value: "영업·고객상담" },
    { index: 5, value: "IT·인터넷" },
    { index: 6, value: "연구개발·설계" },
    { index: 7, value: "건설" },
    { index: 8, value: "마케팅·광고·홍보" },
    { index: 9, value: "생산·제조" },
    { index: 10, value: "의료" },
    { index: 11, value: "무역·유통" },
    { index: 12, value: "디자인" },
    { index: 13, value: "교육" },
    { index: 14, value: "서비스" },
];
const smokes = [
    { index: 1, value: "비흡연" },
    { index: 2, value: "흡연" },
];

const job = [...Array(1).keys()].map((el) => jobs[random(jobs.length)]); // 직업
const religion = [...Array(1).keys()].map((el) => religions[random(religions.length)]); // 종교
const height = "165"; // 키
const body = [...Array(1).keys()].map((el) => bodies[random(bodies.length)]); // 체형
const charm = [...Array(3).keys()].map((el) => charms[random(charms.length)]); // 매력

const UserDetail = ({ route, navigation }) => {
    const { navigate } = navigation;
    const [store, updateStore] = useStore();
    const { width } = route.params; // 가로값 가져오기
    const { userData } = route.params; // 메인 페이지에서 받아오는 회원 data
    const square = parseInt(width); // 기존 가로값 가져오기

    const [userRating, setUserRating] = useState();

    return (
        <ScrollView style={{ padding: 0 }}>
            {/* 프로필 이미지 영역 */}
            <View style={{ width: square, height: square, position: "relative" }}>
                <Swiper
                    showsButtons={false}
                    paginationStyle={{
                        position: "absolute",
                        bottom: 20,
                        left: 0,
                        height: 24,
                        justifyContent: "center",
                    }}
                    dotStyle={{ width: 10, height: 10, borderRadius: 5 }}
                    dotColor={"#FFF"}
                    activeDotStyle={{ width: 10, height: 10, borderRadius: 5 }}
                    activeDotColor={"#7D14DE"}
                >
                    {userData.images.map((image, i) => {
                        return (
                            <View key={`profile${i}`} style={{ width: "100%", height: "100%" }}>
                                <Image
                                    source={{ url: image }}
                                    style={{ width: "100%", height: "100%" }}
                                />
                            </View>
                        );
                    })}
                </Swiper>
            </View>

            <Content style={[Common.flexColumn]}>
                {/* 이름, 나이, 위치 그룹 */}
                <View
                    style={[
                        Common.flexRow,
                        Common.flexBetween,
                        Common.flexWrap,
                        { height: 35, marginBottom: 35 },
                    ]}
                >
                    <View style={[Common.flexRow, { flex: 1, alignItems: "center" }]}>
                        <IcoItem style={{ marginRight: 10 }} />
                        <Text style={{ fontSize: 24 }}>
                            {userData.nickname}({userData.age})
                        </Text>
                    </View>
                    <View
                        style={[
                            Common.flexRow,
                            { flex: 1, alignItems: "center", justifyContent: "flex-end" },
                        ]}
                    >
                        <IcoLocation style={{ marginRight: 10 }} />
                        <Text>{userData.location}km</Text>
                    </View>
                </View>

                {/* 좋아요, 메세지, 번개팅 버튼 그룹 */}
                <View
                    style={[
                        Common.itemCenter,
                        {
                            overflow: "hidden",
                            height: 50,
                            borderRadius: 4,
                            marginBottom: 50,
                        },
                    ]}
                >
                    <LinearGradient
                        style={[Common.gradient, Common.flexRow]}
                        start={[0, 1]}
                        end={[1, 1]}
                        colors={["#FFBF3E", "#FF3366", "#A100C4"]}
                    >
                        <Button
                            style={[
                                Common.flexRow,
                                Common.itemCenter,
                                styles.textButton,
                                { flex: 1 },
                            ]}
                        >
                            <IcoLike style={{ marginRight: 10 }} />
                            <Text style={[styles.buttonInText]}>좋아요</Text>
                        </Button>
                        <Button
                            style={[
                                Common.flexRow,
                                Common.itemCenter,
                                styles.textButton,
                                { flex: 1 },
                            ]}
                        >
                            <IcoMsg style={{ marginRight: 10 }} />
                            <Text style={[styles.buttonInText]}>메시지</Text>
                        </Button>
                        <Button
                            style={[
                                Common.flexRow,
                                Common.itemCenter,
                                styles.textButton,
                                { flex: 1 },
                            ]}
                        >
                            <IcoLightning style={{ marginRight: 10 }} />
                            <Text style={[styles.buttonInText]}>번개팅</Text>
                        </Button>
                    </LinearGradient>
                </View>

                {/* 별점 */}
                <View
                    style={[
                        Common.itemCenter,
                        {
                            height: 40,
                            marginBottom: 40,
                        },
                    ]}
                >
                    <Rating
                        type="star"
                        imageSize={40}
                        showRating={false}
                        startingValue={0}
                        style={{ paddingHorizontal: 10, width: 200 }}
                        onFinishRating={(rating) => console.log(rating)}
                    />
                </View>

                {/* 불량회원 신고하기 */}
                <View style={[Common.itemCenter, { flex: 1 }]}>
                    <Button>
                        <Text style={{ fontSize: 13, color: "#C3C9D4" }}>
                            불량회원으로 신고하기
                        </Text>
                    </Button>
                </View>
            </Content>
            {/* user 전체 정보 슬라이드 컨텐츠 */}
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    textButton: {
        paddingHorizontal: 16,
    },
    buttonInText: {
        fontSize: 14,
        color: "#FFF",
    },
});

export default UserDetail;
