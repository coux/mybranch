import React, { useState, useEffect } from "react";
import { SafeAreaView, View, FlatList, StyleSheet, Text, TouchableOpacity,TouchableHighlight } from "react-native";

import { useUpload, useStore } from "../components/Hooks";
import { GET_STORE_ITEMS } from "../store/queries";
import { useQuery } from "@apollo/react-hooks";
import EStyleSheet from "react-native-extended-stylesheet";
import Content from "../components/Content";
import Button from "../components/Button";
import Common from "../assets/Style";
import IcTrashBox from "../assets/images/common/ic-trashBox.png";
import FetchView from "../components/FetchView";
import { SwipeListView } from 'react-native-swipe-list-view';

// import MessageList from "../components/MessageList";

const MyNotice = (props) => {
    const { navigation } = props;
    
    // const { error, loading, data } = useQuery(GET_STORE_ITEMS);
    // if (loading || error) {
    //     return <FetchView loading={loading} error={error} />;
    // }
    // const { items } = data;

    const [listData, setListData] = useState(
        Array(20)
            .fill('')
            .map((_, i) => ({ key: `${i}`, text: `item #${i}`, name: `${i}호 운영자` }))
    );

    const closeRow = (rowMap, rowKey) => {
        if (rowMap[rowKey]) {
            rowMap[rowKey].closeRow();
        }
    };

    const deleteRow = (rowMap, rowKey) => {
        closeRow(rowMap, rowKey);
        const newData = [...listData];
        const prevIndex = listData.findIndex(item => item.key === rowKey);
        newData.splice(prevIndex, 1);
        setListData(newData);
    };

    const onRowDidOpen = rowKey => {
        console.log('This row opened', rowKey);
    };

    const renderItem = data => (
        <TouchableHighlight
            onPress={() => console.log('You touched me')}
            style={styles.rowFront}
            underlayColor={'#AAA'}
        >
            <View style={styles.textWrap}>
                <Text 
                    style={styles.titleWrap}
                    numberOfLines={1}    
                >
                    {data.item.text} [공지] 이벤트 알림입니다. 회원 여러분의 성원에 감사드리며
                </Text>
                <Text>
                    {data.item.name}
                </Text>
            </View>
        </TouchableHighlight>
    );

    const renderHiddenItem = (data, rowMap) => (
        <View style={styles.rowBack}>
            <TouchableOpacity
                style={[styles.backRightBtn, styles.backRightBtnRight]}
                onPress={() => deleteRow(rowMap, data.item.key)}
            >
                <Text style={styles.backTextWhite}>삭제</Text>
            </TouchableOpacity>
        </View>
    );

    return (
        <View style={[styles.container]}>
            <SwipeListView
                data={listData}
                renderItem={renderItem}
                renderHiddenItem={renderHiddenItem}
                leftOpenValue={0}
                rightOpenValue={-75}
                previewRowKey={'0'}
                previewOpenValue={-40}
                previewOpenDelay={3000}
                onRowDidOpen={onRowDidOpen}
            />
        </View> 
    );
};

export default MyNotice;

const styles = EStyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    backTextWhite: {
        color: '#FFF',
    },
    rowFront: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderBottomColor: '#f3f3f3',
        borderBottomWidth: 1,
        justifyContent: 'center',
        height: 50,
    },
    textWrap: {
        display: 'flex',
        flexDirection: 'row'
    },
    titleWrap: {
        maxWidth: '70%',
        paddingRight: 15
    },
    rowBack: {
        alignItems: 'center',
        backgroundColor: 'white',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 15,
    },
    backRightBtn: {
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        width: 75,
    },
    backRightBtnLeft: {
        backgroundColor: 'blue',
        right: 75,
    },
    backRightBtnRight: {
        backgroundColor: 'red',
        right: 0,
    },
});
