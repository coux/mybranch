import React from "react";
import { View, Image, Text, StyleSheet } from "react-native";
import { useStore } from "../components/Hooks";
import Button from "../components/Button";
import Content from "../components/Content";
import Common from "../assets/Style";

const Enter = ({ navigation }) => {
    const { navigate } = navigation;
    const [store, updateStore] = useStore();
    const { me } = store;

    return (
        <Content>
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <Button onPress={() => navigate("Home")}>
                    <Text style={styles.linkText}>메인페이지</Text>
                </Button>
                <Button onPress={() => navigate("Login")}>
                    <Text style={styles.linkText}>로그인</Text>
                </Button>
                <Button onPress={() => navigate("Join")}>
                    <Text style={styles.linkText}>회원가입</Text>
                </Button>
                <Button onPress={() => navigate("MyDetail")}>
                    <Text style={styles.linkText}>프로필수정</Text>
                </Button>
                <Button onPress={() => navigate("MyPhoto")}>
                    <Text style={styles.linkText}>사진등록수정</Text>
                </Button>
                <Button onPress={() => navigate("MyFavor")}>
                    <Text style={styles.linkText}>이상형설정</Text>
                </Button>
                <Button onPress={() => navigate("MyPage")}>
                    <Text style={styles.linkText}>마이페이지</Text>
                </Button>
                <Button onPress={() => navigate('Shop')}>
                    <Text style={styles.linkText}>상점</Text>
                </Button>
                <Button onPress={() => navigate('Settings')}>
                    <Text style={styles.linkText}>설정 페이지</Text>
                </Button>
                <Button onPress={() => navigate('UserDetail')}>
                    <Text style={styles.linkText}>사용자 프로필 페이지</Text>
                </Button>
                <Button onPress={() => navigate('MyMessage')}>
                    <Text style={styles.linkText}>쪽지 페이지</Text>
                </Button>
                <Button onPress={() => navigate('MyNotice')}>
                    <Text style={styles.linkText}>알림 페이지</Text>
                </Button>
            </View>
        </Content>
    );
};

const styles = StyleSheet.create({
    linkText: {
        height: 30,
        fontSize: 14,
    },
});

export default Enter;
