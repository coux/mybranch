import React, { useState, useEffect, useLayoutEffect } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { useStore } from '../components/Hooks';
import { View, Text, ImageBackground } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { LOGIN } from '../store/queries';
import Content from '../components/Content';
import EStyleSheet from 'react-native-extended-stylesheet';
import TextInput from '../components/InputText';
import Header from '../components/Header';
import Button from '../components/Button';
import Divider from '../components/Divider';
import Common from '../assets/Style';
// import { defaultHeader } from '../navigation/AppNavigator';

const Login = ({ mutate, navigation }) => {
    const { navigate } = navigation;
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [store, updateStore] = useStore();
    const [login, { data }] = useMutation(LOGIN);
    useEffect(() => {
        if (data) {
            const { login } = data;
            updateStore({ me: login });
            navigate('Home');
        }
    }, [data]);

    // useLayoutEffect(() => {
    //     let header = defaultHeader(navigation);
    //     header.headerTitle = '로그인'
    //     navigation.setOptions(header);
    // }, []);

    return (
        <Content>
            <TextInput
                containerStyle={{ marginBottom: 10 }}
                style={Common.textInput}
                placeholder="E-mail"
                onChangeText={setEmail}
                value={email}
                autoCapitalize="none"
                keyboardType="email-address"
                clearButton={true}
            />
            <TextInput
                containerStyle={{ marginBottom: 10 }}
                style={Common.textInput}
                placeholder="Password"
                onChangeText={setPassword}
                value={password}
                clearButton={true}
                secureButton={true}
                secureTextEntry={true}
            />
            <Button
                style={{ ...Common.defaultButton, ...Common.roundedButton }}
                onPress={() => {
                    login({ variables: { email, password } });
                }}
            >
                <LinearGradient
                    style={Common.gradient}
                    start={[0, 1]}
                    end={[1, 1]}
                    colors={['#FFBF3E', '#FF3366', '#A100C4']}
                >
                    <Text adjustsFontSizeToFit style={styles.loginText}>
                        로그인
                    </Text>
                </LinearGradient>
            </Button>

            <Button
                style={Common.iconButton}
                onPress={() => navigate('Social', { uri: `${store.host}/auth/facebook` })}
            >
                <ImageBackground
                    style={Common.imageBackground}
                    imageStyle={styles.icon}
                    resizeMode="contain"
                    source={require('../assets/images/facebook.png')}
                >
                    <Text style={Common.text16rem}>페이스북 계정으로 로그인</Text>
                </ImageBackground>
            </Button>
            <View style={styles.wrapper}>
                <View style={{ ...Common.centerHorizontal, paddingVertical: 10 }}>
                    <Text style={{ color: '#7B8088' }}>계정을 잊으셨나요?</Text>
                    <Button style={{ marginLeft: 4 }}>
                        <Text style={Common.text14rem}>계정찾기</Text>
                    </Button>
                </View>

                <Button onPress={() => navigation.navigate('Join')}>
                    <Text style={Common.text14rem}>회원가입</Text>
                </Button>
            </View>
        </Content>
    );
};

const styles = EStyleSheet.create({
    icon: {
        width: '30rem',
        marginLeft: 20
    },
    loginText: {
        fontSize: '20rem',
        color: '#fff',
        padding: '14rem',
        textAlign: 'center'
    },
    wrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
        // padding: '20rem'
    },
    snsTitle: {
        paddingVertical: '10rem'
    },
    snsTitleText: {
        textAlign: 'center',
        fontSize: '13rem',
        marginBottom: 10,
        color: '#7B8088'
    }
});

// Login.navigationOptions = ({ navigation: { navigate } }) => ({
//     header: () => <Header title="회원가입" titleStyle={Common.headerTitle} containerStyle={Common.header} />
// });

export default Login;
