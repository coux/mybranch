import React, { useEffect } from 'react';
import { View, Image, Text } from 'react-native';
import { useMutation } from '@apollo/react-hooks';
import { useStore } from '../components/Hooks';
import Header from '../components/Header';
import Button from '../components/Button';
import Content from '../components/Content';
import Common from '../assets/Style';
import Modal from 'react-native-modal';
import ArrowLeft from '../assets/images/common/arrow-left';
import Branch from '../assets/images/common/branch-text';
import RoundLogo from '../assets/images/common/logo-round-shape';
import FetchView from '../components/FetchView';

const Enter = ({ navigation }) => {
    const { navigate } = navigation;
    const [store, updateStore] = useStore();
    const { me } = store;

    return (
        <Content>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <RoundLogo />
                <Text style={{ fontSize: 18, marginVertical: 20 }}>회원가입이 완료 되었습니다.</Text>
                {me && (
                    <Text style={{ fontSize: 14, color: '#7D14DE', marginVertical: 20 }}>{`${me.nickname ||
                        me.email} 회원님 이제부터 브렌치의\n 서비스를 이용하실 수 있습니다.`}</Text>
                )}
                <Button
                    style={{ ...Common.defaultButton, ...Common.roundedButton, width: '100%', backgroundColor: '#7D14DE', marginVertical: 20 }}
                    onPress={() => navigate('Home')}
                >
                    <Text adjustsFontSizeToFit style={{ color: '#fff' }}>
                        메인으로
                    </Text>
                </Button>
                <Text
                    style={{ color: '#AFAFAF' }}
                >{`그룹장이 되어 활동해보세요!\n그룹원과 타 그룹원이 커플이 될 경우 리워드를 얻을 수 있습니다.\n친구초대코드를 통해 그룹원을 모집하고, 광고를 통해 리워드를\n얻으실 수 있습니다.`}</Text>
            </View>
        </Content>
    );
};

// Enter.navigationOptions = ({ navigation: { navigate } }) => ({
//     header: <Header title={<Branch />} titleStyle={Common.headerTitle} containerStyle={Common.header} />
// });

export default Enter;
