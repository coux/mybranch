import React, { useState, useEffect } from "react";
import { View, Image, Text, StyleSheet, AsyncStorage } from "react-native";
import { useLazyQuery, useMutation } from "@apollo/react-hooks";
import { useStore, useAsyncEffect, useAsyncStorage } from "../components/Hooks";
import { LOGOUT, GET_NOTIFICATION, SET_NOTIFICATION } from "../store/queries"; // logout
import Button from "../components/Button";
import Content from "../components/Content";
import Common from "../assets/Style";
import ArrowRight from "../assets/images/common/arrow-right-gray";
import SwitchToggle from "react-native-switch-toggle";

const notifications = ["active", "like", "chat", "rating", "match", "location"];

export const getStorage = async (list) => {
    list = Array.isArray(list) ? list : [list];
    const output = await list.reduce(async (promise, cur) => {
        let acc = await promise.then();
        acc[cur] = JSON.parse(await AsyncStorage.getItem(`@${cur}`));
        return Promise.resolve(acc);
    }, Promise.resolve({}));
    return output;
};

const Settings = ({ navigation }) => {
    const { navigate } = navigation;
    const [store, updateStore] = useStore();
    const { me } = store;
    const [logout, { data }] = useMutation(LOGOUT);

    const [getNoti, { data: getNotiRes }] = useLazyQuery(GET_NOTIFICATION);
    const [setNoti, { data: setNotiRes }] = useMutation(SET_NOTIFICATION);

    const [active, setActive] = useState(); // 전체 알림 허용
    const [like, setLike] = useState(); // 좋아요 알림
    const [chat, setChat] = useState(); // 채팅 알림
    const [rating, setRating] = useState(); // 회원평가 알림
    const [match, setMatch] = useState(); // 소개팅 알림
    const [location, setLocation] = useState(); // 위치알림 허용

    useEffect(() => {
        if (data) {
            const { logout } = data;
            updateStore({ me: logout });
        }
    }, [data]);

    useAsyncEffect(async () => {
        const { active, like, chat, rating, match, location } = await getStorage(notifications);
        // 알림 설정에 대한 스토리지 내용을 가져온다
        const undetermined = [active, like, chat, rating, match, location].every(
            (val) => val === null
        );
        // 알림 설정 내역이 스토리지에 없다면
        if (undetermined) {
            getNoti();
        } else {
            setActive(active); // 전체 알림 허용
            setLike(like);
            setChat(chat);
            setRating(rating);
            setMatch(match);
            setLocation(location);
        }
    }, []);

    // 서버로 부터 알림 설정을 받아옴
    useEffect(() => {
        if (getNotiRes) {
            const { userNotification } = getNotiRes;
            if (userNotification) {
                const { active, like, chat, rating, match, location } = userNotification;
                setActive(active); // 전체 알림 허용
                setLike(like);
                setChat(chat);
                setRating(rating);
                setMatch(match);
                setLocation(location);
            }
        }
    }, [getNotiRes]);

    //전체 알림 허용
    useAsyncEffect(async () => {
        if (active !== undefined) {
            const storage = await getStorage(notifications);
            if (storage.active !== active) {
                await AsyncStorage.setItem("@active", JSON.stringify(active));
                setNoti({ variables: { active } });
            }
        }
    }, [active]);

    // 좋아요 알림
    useAsyncEffect(async () => {
        if (like !== undefined) {
            const storage = await getStorage(notifications);
            if (storage.like !== like) {
                await AsyncStorage.setItem("@like", JSON.stringify(like));
                setNoti({ variables: { like } });
            }
        }
    }, [like]);

    // 채팅 알림
    useAsyncEffect(async () => {
        if (chat !== undefined) {
            const storage = await getStorage(notifications);
            if (storage.chat !== chat) {
                await AsyncStorage.setItem("@chat", JSON.stringify(chat));
                setNoti({ variables: { chat } });
            }
        }
    }, [chat]);

    // 회원평가 알림
    useAsyncEffect(async () => {
        if (rating !== undefined) {
            const storage = await getStorage(notifications);
            if (storage.rating !== rating) {
                await AsyncStorage.setItem("@rating", JSON.stringify(rating));
                setNoti({ variables: { rating } });
            }
        }
    }, [rating]);

    // 소개팅 알림
    useAsyncEffect(async () => {
        if (match !== undefined) {
            const storage = await getStorage(notifications);
            if (storage.match !== match) {
                console.log(match);
                setNoti({ variables: { match } });
                await AsyncStorage.setItem("@match", JSON.stringify(match));
            }
        }
    }, [match]);

    //위치허용 알림
    useAsyncEffect(async () => {
        if (location !== undefined) {
            const storage = await getStorage(notifications);
            if (storage.location !== location) {
                await AsyncStorage.setItem("@location", JSON.stringify(location));
                setNoti({ variables: { location } });
            }
        }
    }, [location]);

    return (
        <Content style={{ paddingVertical: 5, paddingHorizontal: 0 }}>
            <View style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>전체 알림 허용</Text>
                <SwitchToggle
                    type={0}
                    containerStyle={styles.switchContainer}
                    circleStyle={styles.switchCircle}
                    switchOn={active}
                    onPress={() => setActive(!active)}
                    circleColorOn="#FFFFFF"
                    backgroundColorOn="#8D31E2"
                    backgroundColorOff="#C3C9D4"
                    duration={300}
                />
            </View>
            <View style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>좋아요 알림</Text>
                <SwitchToggle
                    type={0}
                    containerStyle={styles.switchContainer}
                    circleStyle={styles.switchCircle}
                    switchOn={like}
                    onPress={() => setLike(!like)}
                    circleColorOn="#FFFFFF"
                    backgroundColorOn="#8D31E2"
                    backgroundColorOff="#C3C9D4"
                    duration={300}
                />
            </View>
            <View style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>채팅 알림</Text>
                <SwitchToggle
                    type={0}
                    containerStyle={styles.switchContainer}
                    circleStyle={styles.switchCircle}
                    switchOn={chat}
                    onPress={() => setChat(!chat)}
                    circleColorOn="#FFFFFF"
                    backgroundColorOn="#8D31E2"
                    backgroundColorOff="#C3C9D4"
                    duration={300}
                />
            </View>
            <View style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>회원평가 알림</Text>
                <SwitchToggle
                    type={0}
                    containerStyle={styles.switchContainer}
                    circleStyle={styles.switchCircle}
                    switchOn={rating}
                    onPress={() => setRating(!rating)}
                    circleColorOn="#FFFFFF"
                    backgroundColorOn="#8D31E2"
                    backgroundColorOff="#C3C9D4"
                    duration={300}
                />
            </View>
            <View style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>소개팅 알림</Text>
                <SwitchToggle
                    type={0}
                    containerStyle={styles.switchContainer}
                    circleStyle={styles.switchCircle}
                    switchOn={match}
                    onPress={() => setMatch(!match)}
                    circleColorOn="#FFFFFF"
                    backgroundColorOn="#8D31E2"
                    backgroundColorOff="#C3C9D4"
                    duration={300}
                />
            </View>
            <View style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>위치알림 허용</Text>
                <SwitchToggle
                    type={0}
                    containerStyle={styles.switchContainer}
                    circleStyle={styles.switchCircle}
                    switchOn={location}
                    onPress={() => setLocation(!location)}
                    circleColorOn="#FFFFFF"
                    backgroundColorOn="#8D31E2"
                    backgroundColorOff="#C3C9D4"
                    duration={300}
                />
            </View>
            <Button style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>아는사람 만나지 않기</Text>
                <ArrowRight width={24} height={24} />
            </Button>
            <Button style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>서비스 이용약관</Text>
                <ArrowRight width={24} height={24} />
            </Button>
            <Button style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>개인정보 취급방침</Text>
                <ArrowRight width={24} height={24} />
            </Button>
            <Button
                style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}
                onPress={logout}
            >
                <Text style={[Common.text14rem]}>로그아웃</Text>
                <ArrowRight width={24} height={24} />
            </Button>
            <Button style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>회원탈퇴</Text>
                <ArrowRight width={24} height={24} />
            </Button>
            <View style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>버전정보</Text>
                <Text style={[Common.text14rem,{color:"#C3C9D4"}]}>1.2.1</Text>
            </View>
        </Content>
    );
};

const styles = StyleSheet.create({
    switchContainer: {
        width: 36,
        height: 18,
        borderRadius: 9,
        backgroundColor: "#C3C9D4",
        padding: 4,
    },
    switchCircle: {
        width: 10,
        height: 10,
        borderRadius: 5,
        backgroundColor: "#FFFFFF",
    },
});

export default Settings;
