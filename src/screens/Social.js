import React, { useState, useCallback } from 'react';
import { WebView } from 'react-native-webview';
import { View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ArrowLeft from '../assets/images/common/arrow-left';
import Header from '../components/Header';
import Button from '../components/Button';
import Common from '../assets/Style';
import { NavigationEvents } from 'react-navigation';
import { SkypeIndicator } from 'react-native-indicators';

const Social = ({ navigation }) => {
    const [url, setUrl] = useState({ html: '' });
    const [indicator, setIndicator] = useState(false);

    const onNavigationStateChange = state => {
        //alert(state.url);
    };

    return (
        <View style={styles.container}>
            <NavigationEvents
                onDidFocus={() => {
                    let uri = navigation.getParam('uri');
                    setUrl({ uri });
                }}
                onDidBlur={() => {
                    setUrl({ html: '' });
                }}
            />
            <WebView
                style={styles.webview}
                cacheEnabled={true}
                originWhitelist={['*']}
                onLoadStart={() => setIndicator(true)}
                onLoadEnd={() => setIndicator(false)}
                onNavigationStateChange={onNavigationStateChange}
                source={url}
                useWebKit={true}
            />
        </View>
    );
};

const styles = EStyleSheet.create({
    container: {
        flex: 1
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    webview: {
        flex: 1
    }
});

// Social.navigationOptions = ({ navigation }) => ({
//     header: (
//         <Header title="로그인" titleStyle={Common.headerTitle} containerStyle={Common.header}>
//             <Button
//                 onPress={() => {
//                     navigation.navigate('Login');
//                 }}
//             >
//                 <ArrowLeft />
//             </Button>
//         </Header>
//     )
// });

export default Social;
