import React, { useState, useEffect } from "react";
import { View, Image, Text, StyleSheet } from "react-native";
import { useMutation } from "@apollo/react-hooks";
import { useStore } from "../components/Hooks";
import { LOGOUT } from "../store/queries"; // logout
import Button from "../components/Button";
import Content from "../components/Content";
import Common from "../assets/Style";
import ArrowRight from "../assets/images/common/arrow-right-gray";
import ToggleSwitch from "toggle-switch-react-nativenpm i toggle-switch-react-native --save"; //toggle switch

const Setting = ({ navigation }) => {
    const { navigate } = navigation;
    const [store, updateStore] = useStore();
    const { me } = store;
    const [logout, { data }] = useMutation(LOGOUT);
    useEffect(() => {
        if (data) {
            const { logout } = data;
            updateStore({ me: logout });
        }
    }, [data]);

    return (
        <Content style={{ padding: 0 }}>
            <Button style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>좋아요 알림</Text>
                <ToggleSwitch
                    isOn={false}
                    onColor="#8D31E2"
                    offColor="#C3C9D4"
                    label="Example label"
                    labelStyle={{ color: "black", fontWeight: "900" }}
                    size="small"
                    onToggle={(isOn) => console.log("changed to : ", isOn)}
                />
            </Button>
            <Button style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>아는사람 만나지 않기</Text>
                <ArrowRight width={24} height={24} />
            </Button>
            <Button style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>서비스 이용약관</Text>
                <ArrowRight width={24} height={24} />
            </Button>
            <Button style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>개인정보 취급방침</Text>
                <ArrowRight width={24} height={24} />
            </Button>
            <Button
                style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}
                onPress={logout}
            >
                <Text style={[Common.text14rem]}>로그아웃</Text>
                <ArrowRight width={24} height={24} />
            </Button>
            <Button style={[Common.flexRow, Common.flexBetween, Common.textButton, { height: 44 }]}>
                <Text style={[Common.text14rem]}>회원탈퇴</Text>
                <ArrowRight width={24} height={24} />
            </Button>
        </Content>
    );
};

const styles = StyleSheet.create({});

export default Setting;
