import React, { useState, useEffect } from "react";
import { SafeAreaView, View, FlatList, StyleSheet, Text, TouchableOpacity,TouchableHighlight, Dimensions } from "react-native";
import { TabView, TabBar } from 'react-native-tab-view';

import { useUpload, useStore } from "../components/Hooks";
import { GET_STORE_ITEMS } from "../store/queries";
import { useQuery } from "@apollo/react-hooks";
import EStyleSheet from "react-native-extended-stylesheet";
import Content from "../components/Content";
import Button from "../components/Button";
import Common from "../assets/Style";
import IcTrashBox from "../assets/images/common/ic-trashBox.png";
import FetchView from "../components/FetchView";
import { SwipeListView } from 'react-native-swipe-list-view';

// import MessageList from "../components/MessageList";

const FirstRoute = () => (
    <View style={[styles.scene, { backgroundColor: '#ff4081' }]} />
  );
  
const SecondRoute = () => (
    <View style={[styles.scene, { backgroundColor: '#673ab7' }]} />
);

const initialLayout = { width: Dimensions.get('window').width };

const MyMessage = (props) => {
    const { navigation } = props;
    
    // const { error, loading, data } = useQuery(GET_STORE_ITEMS);
    // if (loading || error) {
    //     return <FetchView loading={loading} error={error} />;
    // }
    // const { items } = data;

    //탭바
    const renderTabBar = props => (
        <TabBar
          {...props}
          indicatorStyle={{ backgroundColor: 'white' }}
          style={{ backgroundColor: '#C3C9D4' }}
          activeColor='#7D14DE'
          indicatorStyle={{ backgroundColor: '#7D14DE'}}
        />
      );

    //탭뷰
    const [index, setIndex] = React.useState(0);
    const [routes] = React.useState([
      { key: 'first', title: '받은 쪽지' },
      { key: 'second', title: '보낸 쪽지' },
    ]);
  
    const renderScene = ({ route }) => {
        switch (route.key) {
          case 'first':
            return  <SwipeListView
                        data={listData}
                        renderItem={renderItem}
                        renderHiddenItem={renderHiddenItem}
                        leftOpenValue={0}
                        rightOpenValue={-75}
                        previewRowKey={'0'}
                        previewOpenValue={-40}
                        previewOpenDelay={3000}
                        onRowDidOpen={onRowDidOpen}
                    />;
          case 'second':
            return <SwipeListView
                        data={listData}
                        renderItem={renderItem}
                        renderHiddenItem={renderHiddenItem}
                        leftOpenValue={0}
                        rightOpenValue={-75}
                        previewRowKey={'0'}
                        previewOpenValue={-40}
                        previewOpenDelay={3000}
                        onRowDidOpen={onRowDidOpen}
                    />;
          default:
            return null;
        }
      };

    //쪽지 리스트
    const [listData, setListData] = useState(
        Array(20)
            .fill('')
            .map((_, i) => ({ key: `${i}`, text: `item #${i}`, name: `${i}호 보낸이` }))
    );

    const closeRow = (rowMap, rowKey) => {
        if (rowMap[rowKey]) {
            rowMap[rowKey].closeRow();
        }
    };

    const deleteRow = (rowMap, rowKey) => {
        closeRow(rowMap, rowKey);
        const newData = [...listData];
        const prevIndex = listData.findIndex(item => item.key === rowKey);
        newData.splice(prevIndex, 1);
        setListData(newData);
    };

    const onRowDidOpen = rowKey => {
        console.log('This row opened', rowKey);
    };

    const renderItem = data => (
        <TouchableHighlight
            onPress={() => console.log('You touched me')}
            style={styles.rowFront}
            underlayColor={'#AAA'}
        >
            <View style={styles.textWrap}>
                <Text 
                    style={styles.titleWrap}
                    numberOfLines={1}    
                >
                    {data.item.text} 안녕하세요. 이것은 긴 문장으로 쓰고 싶은 쪽지입니다.
                </Text>
                <Text>
                    {data.item.name}
                </Text>
            </View>
        </TouchableHighlight>
    );

    const renderHiddenItem = (data, rowMap) => (
        <View style={styles.rowBack}>
            <TouchableOpacity
                style={[styles.backRightBtn, styles.backRightBtnRight]}
                onPress={() => deleteRow(rowMap, data.item.key)}
            >
                <Text style={styles.backTextWhite}>삭제</Text>
            </TouchableOpacity>
        </View>
    );

    return (
        <View style={[styles.container]}>
            <TabView
                navigationState={{ index, routes }}
                onIndexChange={setIndex}
                renderScene={renderScene}
                swipeEnabled={false}
                renderTabBar={renderTabBar}
            />
        </View> 
    );
};

export default MyMessage;

const styles = EStyleSheet.create({
    container: {
        backgroundColor: 'white',
        flex: 1,
    },
    backTextWhite: {
        color: '#FFF',
    },
    rowFront: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderBottomColor: '#f3f3f3',
        borderBottomWidth: 1,
        justifyContent: 'center',
        height: 50,
    },
    textWrap: {
        display: 'flex',
        flexDirection: 'row'
    },
    titleWrap: {
        maxWidth: '70%',
        paddingRight: 15
    },
    rowBack: {
        alignItems: 'center',
        backgroundColor: 'white',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingLeft: 15,
    },
    backRightBtn: {
        alignItems: 'center',
        bottom: 0,
        justifyContent: 'center',
        position: 'absolute',
        top: 0,
        width: 75,
    },
    backRightBtnLeft: {
        backgroundColor: 'blue',
        right: 75,
    },
    backRightBtnRight: {
        backgroundColor: 'red',
        right: 0,
    },
    scene: {
        flex: 1,
    },
});
