import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';
import { graphql } from '@apollo/react-hoc';
import { GET_FAVORITES } from '../store/queries';
import Header from '../components/Header';
import Button from '../components/Button';
import Content from '../components/Content';
import FetchView from '../components/FetchView';
import ToggleItem from '../components/ToggleItem';
import Common from '../assets/Style';
import ArrowLeft from '../assets/images/common/arrow-left';
import EStyleSheet from 'react-native-extended-stylesheet';

const Favor = ({ data: { loading, error, getFavorites }, navigation }) => {
    if (loading || error) return <FetchView loading={loading} error={error} />;
    const [favorites, setFavorites] = useState(['11111']);

    return (
        <Content>
            <Text>내 매력을 선택해주세요.</Text>
            <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 4 }}>
                {getFavorites.charm.map((item, index) => (
                    <ToggleItem
                        style={styles.item}
                        selectedColor={'#7D14DE'}
                        deselectedColor={'#C3C9D4'}
                        item={item}
                        onChange={({ toggle, id }) => {
                            toggle ? setFavorites([...favorites, id]) : setFavorites(favorites.filter(f => f !== id));
                        }}
                        key={`favor${index}`}
                    ></ToggleItem>
                ))}
            </View>
        </Content>
    );
};

Favor.navigationOptions = ({ navigation: { navigate } }) => ({
    header: (
        <Header title="이상형 설정" titleStyle={Common.headerTitle} containerStyle={Common.header} navigate={navigate}>
            <Button
                onPress={() => {
                    navigate('Login');
                }}
            >
                <ArrowLeft />
            </Button>
        </Header>
    )
});

const styles = EStyleSheet.create({
    item: {
        padding: 10,
        borderRadius: 4,
        borderWidth: 1,
        marginRight: 4,
        marginBottom: 4
    },
    selected: {
        borderColor: '#7D14DE',
        color: '#7D14DE'
    },
    deselected: {
        borderColor: '#C3C9D4',
        color: '#C3C9D4'
    }
});

export default graphql(GET_FAVORITES)(Favor);
