import React, { useState, useEffect } from "react";
import { SafeAreaView, View, FlatList, StyleSheet, Text } from "react-native";
import { useUpload, useStore } from "../components/Hooks";
import { GET_STORE_ITEMS } from "../store/queries";
import { useQuery } from "@apollo/react-hooks";
import EStyleSheet from "react-native-extended-stylesheet";
import Content from "../components/Content";
import Button from "../components/Button";
import Common from "../assets/Style";
import IcTicket from "../assets/images/shop/ic-ticket.png";
import IcGift from "../assets/images/shop/ic-gift.png";
import IcPerson from "../assets/images/shop/ic-person.png";
import IcThunder from "../assets/images/shop/ic-thunder.png";
import IcHeart from "../assets/images/shop/ic-heart.png";
import FetchView from "../components/FetchView";

import ShopItem from "../components/ShopItem";

const Shop = (props) => {
    const { navigation } = props;
    
    const { error, loading, data } = useQuery(GET_STORE_ITEMS);
    if (loading || error) {
        return <FetchView loading={loading} error={error} />;
    }
    const { items } = data;

    return (
        <View style={[styles.container]}>
            <SafeAreaView>
                <FlatList
                    data={items}
                    renderItem={({ item, index }) => (
                        <ShopItem
                            key={`item${index}`}
                            // key={item.id}
                            name={item.name}
                            img={item.image}
                            price={item.price}
                            desc={item.desc}
                            navigation={navigation}
                        />
                    )}
                    keyExtractor={(item) => item.name}
                />
            </SafeAreaView>
        </View> 
    );
};

export default Shop;

const styles = EStyleSheet.create({
    container: {
        padding: 20,
        backgroundColor: '#F0F3F8'
    }
});
