import React, { useState, useEffect } from 'react';
import { View, Text, ScrollView, Dimensions } from 'react-native';
import InputScrollView from 'react-native-input-scroll-view';
import EStyleSheet from 'react-native-extended-stylesheet';
import { useLazyQuery, useMutation } from '@apollo/react-hooks';
import { graphql } from '@apollo/react-hoc';
import { useStore, useModal, useDeleteProps } from '../components/Hooks';
import { GET_USER_FAVOR_SUBJECTS, GET_USER_FAVOR, SET_USER_FAVOR } from '../store/queries';
import FetchView from '../components/FetchView';
import Button from '../components/Button';
import Content from '../components/Content';
import ToggleContent from '../components/common/ToggleContent';
import MenuPicker from '../components/common/MenuPicker';
import DropDown from '../components/common/DropDown';
import Common from '../assets/Style';

const MyFavor = ({ navigation, data }) => {
    const { loading, error, getFavorSubjects: subjects } = data;
    if (loading || error) return <FetchView loading={loading} error={error} />;
    const { navigate } = navigation;
    const [modalType, setModalType] = useState('');

    const [getFavor, { data: getFavorRes }] = useLazyQuery(GET_USER_FAVOR);
    const [setFavor, { data: setFavorRes }] = useMutation(SET_USER_FAVOR);

    const [store, updateStore] = useStore();
    const { me, favor } = store;

    const results = setFavorRes || getFavorRes || {};

    // 연령대 min ~ max
    const [ageStart, setAgeStart] = useState();
    const [ageEnd, setAgeEnd] = useState();

    // 키 min ~ max
    const [heightStart, setHeightStart] = useState();
    const [heightEnd, setHeightEnd] = useState();
    // 체형
    const [body, setBody] = useState();
    // 종교
    const [religion, setReligion] = useState();
    // 흡연
    const [smoke, setSmoke] = useState();
    // 음주
    const [drink, setDrink] = useState();
    // 매력 포인트 (최대3개)
    const [charm, setCharm] = useState([]);
    // 지역 (최대 3개)
    const [region, setRegion] = useState([]);
    // 성격 (최대 3개)
    const [character, setCharacter] = useState([]);
    // 최종학력
    const [education, setEducation] = useState();

    useEffect(() => {
        if (me) {
            getFavor();
        }
    }, [me]);

    useEffect(() => {
        const { userFavor } = results;
        if (userFavor) {
            setAgeStart(userFavor.ageStart.index);
            setAgeEnd(userFavor.ageEnd.index);
            setHeightStart(userFavor.heightStart.index);
            setHeightEnd(userFavor.heightEnd.index);
            setEducation(userFavor.education.index);
            setBody(userFavor.body.index);
            setReligion(userFavor.religion.index);
            setSmoke(userFavor.smoke.index);
            setDrink(userFavor.drink.index);
            setCharm(userFavor.charm);
            setRegion(userFavor.region);
            setCharacter(userFavor.character);
        }
    }, [results]);

    useEffect(() => {
        if (setFavorRes) {
            navigate('Home');
        }
    }, [setFavorRes]);

    return (
        <InputScrollView keyboardShouldPersistTaps="always">
            <Content>
                <View style={[Common.gapBottom20, styles.favorContents]}>
                    <View style={[Common.flexRow, Common.flexAlignStart, Common.flexWrap]}>
                        <View style={[Common.flexRow, { width: '50%' }, { paddingVertical: 5 }]}>
                            <Text style={[Common.text14rem]}>연령대 :</Text>
                            <Text style={[Common.text14rem, { paddingHorizontal: 5 }]}>
                                {subjects.ageStarts[ageStart] && subjects.ageStarts[ageStart].value}
                                ~{subjects.ageEnds[ageEnd] && subjects.ageEnds[ageEnd].value}대
                            </Text>
                        </View>

                        <View style={[Common.flexRow, { width: '50%' }, { paddingVertical: 5 }]}>
                            <Text style={[Common.text14rem]}>키 :</Text>
                            <Text style={[Common.text14rem, { paddingHorizontal: 5 }]}>
                                {subjects.heightStarts[heightStart] &&
                                    subjects.heightStarts[heightStart].value}
                                ~
                                {subjects.heightEnds[heightEnd] &&
                                    subjects.heightEnds[heightEnd].value}
                                cm
                            </Text>
                        </View>

                        <View style={[Common.flexRow, { width: '50%' }, { paddingVertical: 5 }]}>
                            <Text style={[Common.text14rem]}>흡연 :</Text>
                            <Text style={[Common.text14rem, { paddingHorizontal: 5 }]}>
                                {subjects.smokes[smoke] && subjects.smokes[smoke].value}
                            </Text>
                        </View>

                        <View style={[Common.flexRow, { width: '50%' }, { paddingVertical: 5 }]}>
                            <Text style={[Common.text14rem]}>체형 :</Text>
                            <Text style={[Common.text14rem, { paddingHorizontal: 5 }]}>
                                {subjects.bodies[body] && subjects.bodies[body].value}
                            </Text>
                        </View>

                        <View
                            style={[
                                Common.flexRow,
                                Common.flexAlignStart,
                                { width: '50%' },
                                { paddingVertical: 5 }
                            ]}
                        >
                            <Text style={[Common.text14rem]}>음주 :</Text>
                            <Text style={[Common.text14rem, { paddingHorizontal: 5, flex: 1 }]}>
                                {subjects.drinks[drink] && subjects.drinks[drink].value}
                            </Text>
                        </View>

                        <View style={[Common.flexRow, { width: '50%' }, { paddingVertical: 5 }]}>
                            <Text style={[Common.text14rem]}>종교 :</Text>
                            <Text style={[Common.text14rem, { paddingHorizontal: 5 }]}>
                                {subjects.religions[religion] && subjects.religions[religion].value}
                            </Text>
                        </View>
                    </View>

                    <View style={[Common.flexColumn]}>
                        <View style={[Common.flexRow, { paddingVertical: 5 }]}>
                            <Text style={[Common.text14rem]}>지역 :</Text>
                            <Text style={[Common.text14rem, { paddingHorizontal: 5 }]}>
                                {region.map((elment) => elment.value).join(', ')}
                            </Text>
                        </View>
                        <View style={[Common.flexRow, { paddingVertical: 5 }]}>
                            <Text style={[Common.text14rem]}>성격 :</Text>
                            <Text style={[Common.text14rem, { paddingHorizontal: 5 }]}>
                                {character.map((elment) => elment.value).join(', ')}
                            </Text>
                        </View>
                        <View style={[Common.flexRow, { paddingVertical: 5 }]}>
                            <Text style={[Common.text14rem]}>매력 :</Text>
                            <Text style={[Common.text14rem, { paddingHorizontal: 5 }]}>
                                {charm.map((elment) => elment.value).join(', ')}
                            </Text>
                        </View>
                    </View>
                </View>

                <Text style={styles.title}>이상형 설정</Text>
                {/* 연령대 선택 */}
                <DropDown
                    value={[subjects.ageStarts[ageStart], subjects.ageEnds[ageEnd]]}
                    placeholder="연령대 선택"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '연령대 선택',
                        containerStyle: styles.dropMenu,
                        contentStyle: { flexDirection: 'row' }
                    }}
                >
                    <MenuPicker
                        pickerStyle={[styles.picker, { width: '50%' }]}
                        pickerItemStyle={styles.pickerItem}
                        selected={ageStart}
                        onSelect={setAgeStart}
                        items={subjects.ageStarts}
                    />
                    <MenuPicker
                        pickerStyle={[styles.picker, { width: '50%' }]}
                        pickerItemStyle={styles.pickerItem}
                        selected={ageEnd}
                        onSelect={setAgeEnd}
                        items={subjects.ageEnds}
                    />
                </DropDown>
                {/* 키 선택 */}
                <DropDown
                    value={[subjects.heightStarts[heightStart], subjects.heightEnds[heightEnd]]}
                    placeholder="키 선택"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '키 선택',
                        containerStyle: styles.dropMenu,
                        contentStyle: { flexDirection: 'row' }
                    }}
                >
                    <MenuPicker
                        pickerStyle={[styles.picker, { width: '50%' }]}
                        pickerItemStyle={styles.pickerItem}
                        selected={heightStart}
                        onSelect={setHeightStart}
                        items={subjects.heightStarts}
                    />
                    <MenuPicker
                        pickerStyle={[styles.picker, { width: '50%' }]}
                        pickerItemStyle={styles.pickerItem}
                        selected={heightEnd}
                        onSelect={setHeightEnd}
                        items={subjects.heightEnds}
                    />
                </DropDown>
                {/* 체형선택 */}
                <DropDown
                    value={subjects.bodies[body]}
                    placeholder="체형을 선택하세요"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '체형 선택',
                        containerStyle: styles.dropMenu
                    }}
                >
                    <MenuPicker
                        pickerStyle={styles.picker}
                        pickerItemStyle={styles.pickerItem}
                        selected={body}
                        onSelect={setBody}
                        items={subjects.bodies}
                    />
                </DropDown>
                {/* 종교선택 */}
                <DropDown
                    value={subjects.religions[religion]}
                    placeholder="종교를 선택하세요"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '종교 선택',
                        containerStyle: styles.dropMenu
                    }}
                >
                    <MenuPicker
                        pickerStyle={styles.picker}
                        pickerItemStyle={styles.pickerItem}
                        selected={religion}
                        onSelect={setReligion}
                        items={subjects.religions}
                    />
                </DropDown>
                {/* 흡연선택 */}
                <DropDown
                    value={subjects.smokes[smoke]}
                    placeholder="흡연 여부를 선택하세요"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '흡연 여부 선택',
                        containerStyle: styles.dropMenu
                    }}
                >
                    <MenuPicker
                        pickerStyle={styles.picker}
                        pickerItemStyle={styles.pickerItem}
                        selected={smoke}
                        onSelect={setSmoke}
                        items={subjects.smokes}
                    />
                </DropDown>
                {/* 음주선택 */}
                <DropDown
                    value={subjects.drinks[drink]}
                    placeholder="음주 여부를 선택하세요"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '음주량 선택',
                        containerStyle: styles.dropMenu
                    }}
                >
                    <MenuPicker
                        pickerStyle={styles.picker}
                        pickerItemStyle={styles.pickerItem}
                        selected={drink}
                        onSelect={setDrink}
                        items={subjects.drinks}
                    />
                </DropDown>
                {/* 최종학력선택 */}
                <DropDown
                    value={subjects.educations[education]}
                    placeholder="최종학력을 선택하세요"
                    containerStyle={{ marginBottom: 20 }}
                    modal={{
                        type: 'bottom',
                        animationIn: 'slideInUp',
                        animationOut: 'slideOutDown',
                        backdropTransitionOutTiming: 0
                    }}
                    menu={{
                        title: '최종학력 선택',
                        containerStyle: styles.dropMenu
                    }}
                >
                    <MenuPicker
                        pickerStyle={styles.picker}
                        pickerItemStyle={styles.pickerItem}
                        selected={education}
                        onSelect={setEducation}
                        items={subjects.educations}
                    />
                </DropDown>
                {/* 지역선택 */}
                <View style={[Common.gapBottom10]}>
                    <ToggleContent
                        title="지역 (최대 3개 선택)"
                        titleStyle={styles.title}
                        items={subjects.regions}
                        itemStyle={styles.item}
                        containerStyle={styles.toggle}
                        selecteds={region}
                        onChange={setRegion}
                        limit={3}
                    />
                </View>
                {/* 성격선택 */}
                <View style={[Common.gapBottom10]}>
                    <ToggleContent
                        title="성격 (최대 3개 선택)"
                        titleStyle={styles.title}
                        items={subjects.characters}
                        itemStyle={styles.item}
                        containerStyle={styles.toggle}
                        selecteds={character}
                        onChange={setCharacter}
                        limit={3}
                    />
                </View>
                {/* 매력포인트선택 */}
                <View style={[Common.gapBottom10]}>
                    <ToggleContent
                        title="매력포인트 (최대 3개 선택)"
                        titleStyle={styles.title}
                        items={subjects.charms}
                        itemStyle={styles.item}
                        containerStyle={styles.toggle}
                        selecteds={charm}
                        onChange={setCharm}
                        limit={3}
                    />
                </View>
            </Content>
            <View>
                <Button
                    style={{
                        ...Common.defaultButton,
                        ...Common.roundedButton,
                        borderRadius: 0,
                        borderWidth: 1,
                        borderColor: '#7D14DE',
                        backgroundColor: '#7D14DE'
                    }}
                    onPress={() => {
                        setFavor({
                            variables: useDeleteProps(
                                {
                                    ageStart: subjects.ageStarts[ageStart],
                                    ageEnd: subjects.ageEnds[ageEnd],
                                    heightStart: subjects.heightStarts[heightStart],
                                    heightEnd: subjects.heightEnds[heightEnd],
                                    body: subjects.bodies[body],
                                    religion: subjects.religions[religion],
                                    smoke: subjects.smokes[smoke],
                                    drink: subjects.drinks[drink],
                                    education: subjects.educations[education],
                                    character,
                                    region,
                                    charm
                                },
                                '__typename'
                            )
                        });
                    }}
                >
                    <Text style={{ color: '#FFFFFF' }}>완료</Text>
                </Button>
            </View>
        </InputScrollView>
    );
};

export default graphql(GET_USER_FAVOR_SUBJECTS)(MyFavor);

const styles = EStyleSheet.create({
    title: {
        height: '15rem',
        marginBottom: 5,
        fontSize: 12,
        color: '#2A2E34'
    },
    toggle: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 4
    },
    item: {
        padding: 10,
        borderRadius: 4,
        borderWidth: 1,
        marginRight: 4,
        marginBottom: 4
    },
    favorContents: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        backgroundColor: '#F5F4F4'
    },
    dropMenu: {
        marginTop: -3,
        borderWidth: 1,
        borderColor: '#C3C9D4'
    },
    picker: {
        height: 200,
        backgroundColor: '#fff',
        overflow: 'hidden'
    },
    pickerItem: {
        color: '#000',
        fontSize: 20,
        marginTop: -10,
        borderWidth: 0
    }
});
