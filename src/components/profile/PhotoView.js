import React, { useState, useEffect } from 'react';
import { ViewStyle, View, Text, Image } from 'react-native';
import { useImagePicker } from '../Hooks';
import { DotIndicator } from 'react-native-indicators';
import { useStore, useModal } from '../Hooks';
import { ListItem } from 'react-native-elements';
import Modal from 'react-native-modal';
import EStyleSheet from 'react-native-extended-stylesheet';
import Button from '../Button';
import Common from '../../assets/Style';
import IconCamera from '../../assets/images/common/ic_camera';
import IconImages from '../../assets/images/common/ic_images';
import IconFacebook from '../../assets/images/common/ic_facebook';
import CircleClose from '../../assets/images/common/circle_close_btn';
import CirclePlus from '../../assets/images/common/circle_plus_btn';

const PhotoView = (props) => {
    const { label, photo, onAdded, onRemove, containerStyle } = props;
    console.log('photo: ', photo);
    const [modal, setModal] = useState(false);
    // const [store, updateStore] = useStore();
    const [image, { camera, select, cancel }] = useImagePicker({
        cropping: true,
        includeBase64: true,
    });

    useEffect(() => {
        if (image) {
            console.log('image: ', image);
            onAdded(image);
        }
    }, [image]);
    return (
        <View style={{ ...containerStyle, ...styles.container }}>
            <View style={styles.view}>
                <View style={styles.image}>
                    {image || photo ? (
                        <Image
                            style={{
                                width: '100%',
                                height: '100%',
                                resizeMode: 'cover',
                            }}
                            source={image || photo}
                        />
                    ) : (
                        <Text style={styles.label}>{label}</Text>
                    )}
                </View>
            </View>
            <Button
                style={{ position: 'absolute', bottom: -5, right: -5 }}
                onPress={() => (image ? cancel() : setModal(true))}
            >
                {image || photo ? <CircleClose /> : <CirclePlus />}
            </Button>
            <Modal
                isVisible={modal}
                onBackdropPress={() => setModal(false)}
                animationIn="slideInUp"
                animationOut="slideOutDown"
                animationInTiming={300}
                animationOutTiming={0}
                backdropTransitionInTiming={300}
                backdropTransitionOutTiming={0}
                style={styles.bottomModal}
            >
                <View style={{ backgroundColor: '#fff' }}>
                    <Button
                        onPress={() => {
                            setModal(false);
                            camera();
                        }}
                    >
                        <ListItem
                            title="카메라"
                            titleStyle={{ color: '#2A2E34' }}
                            leftIcon={IconCamera}
                            bottomDivider
                            chevron={{ color: '#2E5FBF' }}
                        />
                    </Button>
                    <Button
                        onPress={() => {
                            setModal(false);
                            select();
                        }}
                    >
                        <ListItem
                            title="겔러리"
                            titleStyle={{ color: '#2A2E34' }}
                            leftIcon={IconImages}
                            bottomDivider
                            chevron={{ color: '#2E5FBF' }}
                        />
                    </Button>
                    <ListItem
                        title="페이스북"
                        titleStyle={{ color: '#2A2E34' }}
                        leftIcon={IconFacebook}
                        bottomDivider
                        chevron={{ color: '#2E5FBF' }}
                    />
                </View>
            </Modal>
        </View>
    );
};

export default PhotoView;

const styles = EStyleSheet.create({
    container: {
        aspectRatio: 1,
    },
    view: {
        width: '100%',
        height: '100%',
        backgroundColor: '#F5F4F4',
    },
    image: {
        overflow: 'hidden',
        width: '100%',
        height: '100%',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#C3C9D4',
        alignItems: 'center',
        justifyContent: 'center',
    },
    label: {
        color: '#7B8088',
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0,
    },
});
