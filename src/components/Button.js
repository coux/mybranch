import React, { ReactNode, FunctionComponent } from 'react';
import { ViewStyle, TouchableOpacity } from 'react-native';

const Button = props => {
    return <TouchableOpacity {...props}>{props.children}</TouchableOpacity>;
};

export default Button;
