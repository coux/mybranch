import React, { ReactNode } from 'react';
import { View, Text } from 'react-native';
import { DotIndicator } from 'react-native-indicators';

const ViewFetch = ({ loading, error }) => {
    return (
        <View>
            {loading && <DotIndicator size={4} color="#fff" count={4} />}
            {error && <Text style={{ color: 'red' }}> {`${error}`}</Text>}
        </View>
    );
};

export default ViewFetch;
