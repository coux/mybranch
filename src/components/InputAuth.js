import React, { useEffect } from 'react';
import { Text } from 'react-native';
import { graphql } from '@apollo/react-hoc';
import { START_TIMER, GET_SMS_AUTH_CODE } from '../store/queries';
import { compose } from 'recompose';
import Common from '../assets/Style';
import FetchView from '../components/FetchView';
import TextInput from '../components/InputText';

const InputAuth = ({ data, mutate, setValidateId, setAuthState, setAuthCode }) => {
    const { loading, error, getSmsAuthCode } = data;
    if (loading || error) return <FetchView loading={loading} error={error} />;
    const { id, code } = getSmsAuthCode;
    useEffect(() => {
        mutate({ variables: { id, begin: 10, end: 0 } });
    }, [id]);
    setAuthState('received');
    setAuthCode(code);
    setValidateId(id);

    return (
        <TextInput
            containerStyle={{ flex: 1, marginRight: 5 }}
            style={{ ...Common.textInput }}
            label="인증번호 입력"
            placeholder={`인증 번호를 입력 하세요`}
            value={code}
            editable={false}
        />
    );
};

export default compose(
    graphql(GET_SMS_AUTH_CODE, { options: { fetchPolicy: 'network-only' } }),
    graphql(START_TIMER)
)(InputAuth);
