import React, { useEffect, useCallback } from "react";
import { ViewStyle, View, Text } from "react-native";
import { useMutation } from "@apollo/react-hooks";
import { useStore, usePermission } from "../components/Hooks";
import { LOGOUT, SET_USER_LOCATION } from "../store/queries";
import { useNavigation } from "@react-navigation/native";
import * as Permissions from "expo-permissions";
import * as Location from "expo-location";
import EStyleSheet from "react-native-extended-stylesheet";
import Button from "./Button";
import Common from "../assets/Style";

const User = (props) => {
    const [store, updateStore] = useStore();
    const { navigate } = useNavigation();
    const { me } = store;
    const [logout, { data }] = useMutation(LOGOUT);
    const [updateLocation] = useMutation(SET_USER_LOCATION);

    const askPermissons = usePermission(Permissions.LOCATION);
    const grantedLocation = useCallback(async () => {
        /* 사용자의 위치 정보를 요청 서버 전송 */
        let location = await Location.getCurrentPositionAsync({});
        if (location) {
            const {
                coords: { latitude, longitude },
            } = location;
            updateLocation({
                variables: { latitude, longitude },
            });
        }
    }, []);

    const requestLocation = useCallback(async () => {
        /* 필요한 권한을 요청 */
        const granted = await askPermissons(grantedLocation);
        if (granted) {
            grantedLocation();
        }
    }, []);

    useEffect(() => {
        if (data) {
            const { logout } = data;
            updateStore({ me: logout });
        }
    }, [data]);

    useEffect(() => {
        if (me) {
            requestLocation();
        }
    }, [me]);

    return (
        <View style={props.style}>
            {me ? (
                <>
                    <View style={{ flexDirection: "row", justifyContent: "flex-end" }}>
                        <Button onPress={() => navigate("MyDetail")}>
                            <Text style={{ padding: 2 }}>{me.nickname}</Text>
                        </Button>
                        <Button
                            style={{ ...Common.roundedButton, borderWidth: 1, padding: 2 }}
                            onPress={logout}
                        >
                            <Text>로그아웃</Text>
                        </Button>
                    </View>
                    <Text>{me.email}</Text>
                </>
            ) : (
                <Button
                    style={{ ...Common.roundedButton, borderWidth: 1, padding: 2 }}
                    onPress={() => navigate("Login")}
                >
                    <Text>로그인</Text>
                </Button>
            )}
        </View>
    );
};

export default User;
