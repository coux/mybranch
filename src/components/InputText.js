import React, { useState, FunctionComponent } from 'react';
import { View, TextInput, Text, TextStyle, TouchableOpacity } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import ClearIcon from '../assets/images/common/input_clear_btn';
import SecureIcon from '../assets/images/common/input_eye_btn';

const InputText = props => {
    const [text, setText] = useState('');
    const [focus, setFocus] = useState(false);
    const [secure, setSecure] = useState(props.secureTextEntry || false);

    const onFocus = () => {
        setFocus(true);
        props.onFocus && props.onFocus();
    };
    const onBlur = () => {
        setFocus(false);
        props.onBlur && props.onBlur();
    };

    const onChangeText = value => {
        setText(value);
        props.onChangeText && props.onChangeText(value);
    };
    const { containerStyle, label, clearButton, secureButton, editable } = props;

    return (
        <View style={containerStyle}>
            {label && <Text style={styles.title}>{label}</Text>}
            <View style={{ ...styles.wrapper, borderColor: focus ? '#2A2E34' : '#C3C9D4' }}>
                <TextInput
                    {...props}
                    onFocus={onFocus}
                    onBlur={onBlur}
                    onChangeText={onChangeText}
                    secureTextEntry={secure}
                    enablesReturnKeyAutomatically={true}
                />
                {editable && clearButton && text.length > 0 && (
                    <TouchableOpacity onPress={() => onChangeText('')}>
                        <ClearIcon width={25} height={25} />
                    </TouchableOpacity>
                )}
                {editable && secureButton && (
                    <TouchableOpacity onPress={() => setSecure(!secure)}>
                        <SecureIcon width={25} height={25} />
                    </TouchableOpacity>
                )}
            </View>
        </View>
    );
};

InputText.defaultProps = {
    editable: true
};

const styles = EStyleSheet.create({
    wrapper: {
        borderWidth: 1,
        alignItems: 'center',
        flexDirection: 'row',
        borderColor: '#C3C9D4',
        borderRadius: 5,
        paddingHorizontal: 10
    },
    title: {
        height: '15rem',
        marginBottom: 5,
        fontSize: 12,
        color: '#2A2E34'
    }
});

export default InputText;
