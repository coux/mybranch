import React, { useEffect } from 'react';
import { useMutation } from '@apollo/react-hooks';
import { useStore } from '../components/Hooks';
import { Text } from 'react-native';
import { JOIN } from '../store/queries';
import { GET_STATE } from '../store/queries';
import { graphql } from '@apollo/react-hoc';
import Button from '../components/Button';
import Common from '../assets/Style';

const ButtonSign = ({ mutate, email, password, nickname, phone, authState }) => {
    const [join, { data }] = useMutation(JOIN);
    const [store, updateStore] = useStore();
    useEffect(() => {
        if (data) {
            const { join } = data;
            updateStore({ me: join });
        }
    }, [data]);
    return (
        <Button
            style={{ ...Common.defaultButton, backgroundColor: authState === 'done' ? '#7D14DE' : '#C3C9D4' }}
            onPress={() => {
                join({ variables: { email, password, nickname, phone } });
            }}
        >
            <Text style={{ ...Common.text16rem, color: '#fff' }}>회웝가입</Text>
        </Button>
    );
};

export default ButtonSign;
