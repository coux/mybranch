import React, { useState, useEffect, useCallback } from 'react';
import ImagePicker from 'react-native-image-crop-picker';
import * as Permissions from 'expo-permissions';
import { Alert, Animated, Easing, AsyncStorage } from 'react-native';
import { useQuery, useMutation } from '@apollo/react-hooks';
import {
    GET_STATE,
    SET_STATE,
    SINGLE_UPLOAD,
    MULTIPLE_UPLOAD,
} from '../store/queries';

('use strict');
export const useAsyncEffect = (effect, inputs) => {
    useEffect(() => {
        effect();
    }, inputs);
};

export const useStore = () => {
    /* 1. 들어오는 값이 임시 저장됨 */
    const [value, setValue] = useState();
    /* 2. 아폴로 캐시 데이터를 가져옴 */
    const {
        data: { state },
    } = useQuery(GET_STATE);
    /* 3. 아폴로 캐시 데이터를 변경할 함수 세팅 */
    const [setState] = useMutation(SET_STATE);
    /* 4. 1번 값을 감시하고 있다가 변경 되면 동작 시킴 */
    useEffect(() => {
        if (value) {
            /* 캐시에 저장 할수 있도록 리졸버의 포맷에 맞춘다. */
            setState({
                variables: { state: { ...value }, __typename: 'State' },
            });
        }
    }, [value]);
    /* 0. 값이 들어오고 나감 */
    return [state, setValue];
};

export const usePermission = (...args) => {
    /* 요청한 권한들을 인자로 받아옴. 멀티 권한체크 겸용 */
    const askPermissons = useCallback(async (next) => {
        /* 요청한 모든 권한상태를 허용 및 세부 사항으로 가져옴 */
        const { granted, permissions } = await Permissions.getAsync(...args);
        /* 만약 모든 권한중 허용이 아닌 사항만 요청 함 */
        if (!granted) {
            /* 거부된 권한을 요청 함 */
            const { granted } = await Permissions.askAsync(...args);
            /* 요청이 허용되었다면 */
            if (granted) {
                /* 원래 콜백함수 호출 */
                next();
            } else {
                /* 거부되었다면 경고창 띄움*/
                Alert.alert(
                    '권한 획득 실패',
                    '정상적인 앱사용을 위해서 권한을 허용해 주세요.',
                    [
                        {
                            text: '취소',
                            onPress: () => console.log('Cancel Pressed'),
                        },
                        { text: '확인', onPress: () => askPermissons(next) },
                    ],
                    { cancelable: false }
                );
            }
        }
        return granted;
    }, []);
    return askPermissons;
};

/* useUpload에서 분리 */
export const useImagePicker = (options) => {
    /* 이미지들를 임시 저장 */
    const [images, setImages] = useState();
    /* 라이브러러 접근 권한 요청용 */
    const askPermissons = usePermission(
        Permissions.CAMERA,
        Permissions.CAMERA_ROLL
    );
    /* 이미지 선택 시 */
    const select = useCallback(async () => {
        /* 필요한 권한을 요청 */
        const granted = await askPermissons(select);
        /* 모든 권한이 허용 되었다면 */
        if (granted) {
            /* 선택된 이미지 들을 가져온다 */
            let images = await ImagePicker.openPicker(options);
            /* 선택된 이미지가 리스트 형이라면 */
            if (Array.isArray(images)) {
                /* 네이티브 이미지에서 쉽게 불러다 쓸수 있다록 url 속성을 추가해준다 */
                images = images.map((image) => ({ ...image, uri: image.path }));
            } else {
                images = { ...images, uri: images.path };
            }
            setImages(images);
        }
    }, []);

    const camera = useCallback(async () => {
        /* 필요한 권한을 요청 */
        const granted = await askPermissons(camera);
        /* 모든 권한이 허용 되었다면 */
        if (granted) {
            /* 선택된 카메라 기능을 켬 */
            let images = await ImagePicker.openCamera(options);
            /* 반횐된 이미지가 리스트형이라면 */
            if (Array.isArray(images)) {
                /* 네이티브 이미지에서 쉽게 불러다 쓸수 있다록 url 속성을 추가해준다 */
                images = images.map((image) => ({ ...image, uri: image.path }));
            } else {
                images = { ...images, uri: images.path };
            }
            setImages(images);
        }
    }, []);

    /* 만약 선택된 이미지를 삭제 한다면 */
    const cancel = useCallback(async () => {
        /* 이미지 피커의 캐시 이미지를 삭제해주고 */
        await ImagePicker.clean();
        /* 임시 저장용 이미지도 제거 해준다 */
        setImages();
    }, []);
    /* 외부 방출용 */
    return [images, { camera, select, cancel }];
};

export const useUpload = (options) => {
    /* 멀티 업로드 확인용 */
    const { multiple } = options;
    const [store] = useStore();
    const { me } = store;
    if (!me) {
        return;
    }
    const { email } = me;
    /* 파일 업로드 전 겔러리 접근 권한 확인용 */
    const [granted, setGranted] = useState(false);
    /* 업로드 완료후 서버 응답데이터를 담기용 */
    const [results, setResults] = useState();
    /* 개별 이미지 업로드용 */
    const [singleUpload, payload] = useMutation(SINGLE_UPLOAD);
    /* 멀티 이미지 업로드용 */
    const [multipleUpload, payloads] = useMutation(MULTIPLE_UPLOAD);
    /* 파일 선택 버튼을 누르면 실행 */
    const upload = useCallback((images) => {
        /* 선택된 파일이 리스트라면 */
        if (multiple) {
            /* 업로드 가능 포맷으로 변환해 줌 */
            const files = images.map(({ path, filename, mime, data, flag }) => {
                return {
                    uri: path,
                    name: filename,
                    type: mime,
                    stream: data,
                    flag,
                };
            });

            /* 업드로 뮤테이션 실행 */
            multipleUpload({ variables: { files } });
        } else {
            const { path, filename, mime, data } = images;
            /* 업로드 가능 포맷으로 변환해 줌 */
            const file = {
                uri: path,
                name: filename,
                type: mime,
                stream: data,
                flag,
            };
            /* 업드로 뮤테이션 실행 */
            singleUpload({ variables: { file } });
        }
    }, []);
    /* 서버응답 데이터가 들어오면 외부로 내보냄*/
    useEffect(() => {
        const { data } = multiple ? payloads : payload;
        setResults(data);
    }, [payloads, payload]);
    return [results, upload];
};

/* 모달오픈 간소화 훅 */
export const useModal = () => {
    const [data, setData] = useState();
    const [store, updateStore] = useStore();
    const open = useCallback((options) => {
        if (!options) {
            options = { type: '', name: '' };
        }

        updateStore({
            modal: {
                ...options,
                __typename: 'Modal',
            },
        });
    }, []);

    return open;
};

export const useAnimation = ({ animate, duration }) => {
    const [animation, setAnimation] = useState(new Animated.Value(0));

    useEffect(() => {
        Animated.timing(animation, {
            toValue: animate ? 1 : 0,
            duration,
            easing: Easing.exp,
        }).start();
    }, [animate]);

    return animation;
};

export const useDeleteProps = (obj, keys) => {
    if (obj instanceof Array) {
        obj.forEach((item) => {
            useDeleteProps(item, keys);
        });
    } else if (typeof obj === 'object') {
        Object.getOwnPropertyNames(obj).forEach((key) => {
            if (keys.indexOf(key) !== -1) delete obj[key];
            else useDeleteProps(obj[key], keys);
        });
    }
    return obj;
};

//         // let result = await ImagePicker.launchCameraAsync({
//         // let result = await ImagePicker.launchImageLibraryAsync({
//         //     mediaTypes: ImaePicker.MediaTypeOptions.All,
//         //     allowsEditing: true,
//         //     aspect: [4, 3],
//         //     quality: 1,
//         //     allowsMultipleSelection: true
//         // });

// export const useCameraRoll = ({ first = 40, assetType = 'Photos', groupTypes = 'All' }) => {
//     const [photos, setPhotos] = useState([]);
//     const [after, setAfter] = useState(null);
//     const [hasNextPage, setHasNextPage] = useState(true);
//     const getPhotos = useCallback(async () => {
//         if (!hasNextPage) return;
//         const { edges } = await CameraRoll.getPhotos({
//             first,
//             assetType,
//             groupTypes,
//             ...(after && { after })
//         });

//         console.log(edges);
//         // if (after === pageInfo.end_cursor) return;
//         const images = edges.map(i => i.node).map(i => i.image);
//         setPhotos([...photos, ...images]);
//         // setAfter(pageInfo.end_cursor);
//         // setHasNextPage(pageInfo.has_next_page);
//     }, [after, hasNextPage, photos]);
//     return [photos, getPhotos];
// };
