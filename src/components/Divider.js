import React from 'react';
import { View, Text } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
export default (props) => {
	return (
		<View style={{ ...props.containerStyle, ...styles.container }}>
			<Text adjustsFontSizeToFit style={{ ...props.labelStyle, ...styles.text }}>
				{props.label}
			</Text>
		</View>
	);
};

const styles = EStyleSheet.create({
	container: {
		justifyContent: 'center',
		alignItems: 'center'
	},
	text: {
		position: 'absolute',
		backgroundColor: '#fff',
		paddingHorizontal: 6
	}
});
