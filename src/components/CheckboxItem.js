import React, { ReactNode, useState } from "react";
import { Text, TouchableOpacity } from "react-native";
import Common from "../assets/Style";
import CheckboxIcon from "../assets/images/common/Checkbox";
import CheckboxOnIcon from "../assets/images/common/Checkbox_on";

const checkboxItem = ({
    style,
    item,
    onChange
}) => {
    const [toggle, setToggle] = useState(false);

    return (
        <TouchableOpacity
            style={{ ...style, ...Common.flexRow }}
            onPress={() => {
                const value = !toggle;
                setToggle(value);
                onChange({ toggle: value, id: item.id });
            }}
        >
            {toggle ? (
                <CheckboxOnIcon width={18} height={18} />
            ) : (
                <CheckboxIcon width={18} height={18} />
            )}
            <Text style={{ ...Common.text14rem, marginLeft: 5 }}>
                {item.value}
            </Text>
        </TouchableOpacity>
    );
};

export default checkboxItem;
