import React, { useEffect } from 'react';
import { ViewStyle, View, Text, Image, ImageBackground } from 'react-native';
import { useMutation } from '@apollo/react-hooks';
import { useStore } from '../components/Hooks';
import EStyleSheet from 'react-native-extended-stylesheet';
import Button from './Button';
import Common from '../assets/Style';

const ShopItem = props => {
  const { name, image, price, desc, navigation } = props;
  const { navigate } = navigation;
  
  return (
    <View>
      <Button 
        onPress={() => navigate('BuyItem', {
          name: name,
          price: price,
          image:image,
          desc: desc
        })}
        style={[styles.wrapper, Common.flexRow]}
      >
        <View style={styles.iconImgWrap}>
          <Image 
            source={image}
            style={styles.iconImg}
          />
        </View>
        <View style={styles.itemTextWrap}>
          <Text style={styles.itemNameText}>
            {name}
          </Text>
          <Text style={styles.priceText}>
            {price} 원
          </Text>
        </View>
      </Button>
    </View>
  );
};

export default ShopItem;

const styles = EStyleSheet.create({
  wrapper: {
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#D8E1F0',
    marginBottom: 10,
    backgroundColor: 'white'
  },
  iconImgWrap: {
    padding: 24,
    borderRightWidth: 1,
    borderColor: '#D8E1F0',
  },
  iconImg: {
    width: 50,
    height: 50,
    resizeMode: 'contain',
  },
  itemTextWrap: {
    padding: 20
  },
  itemNameText: {
    fontSize: 14,
    lineHeight: 20,
    color: '#2A2E34'
  },
  priceText: {
    fontSize: 18,
    fontWeight: 'bold',
    lineHeight: 26,
    color: '#7D14DE'
  }
});
