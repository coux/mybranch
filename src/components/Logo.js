import React from 'react';
import { Image, View, StyleSheet } from 'react-native';


export default ({ width, height }) => {
	return (
		<View style={styles.container}>
			<Image
				source={require('../assets/images/logo.png')}
				style={{
					width,
					height,
					flex: 1,
					resizeMode: 'contain'
				}}
			/>
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center'
	}
});
