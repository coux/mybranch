import React, { ReactNode, FunctionComponent } from 'react';
import { View, ViewStyle } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

const Content = (props) => {
	return (
		<View {...props} style={{ ...styles.container, ...props.style }}>
			{props.children}
		</View>
	);
};

const styles = EStyleSheet.create({
	container: {
		flex: 1,
		padding: '20rem',
	}
});

export default Content;
