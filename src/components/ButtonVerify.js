import React, { useState } from 'react';
import { Text } from 'react-native';
import { useLazyQuery } from '@apollo/react-hooks';
import { VERIFY_SMS_AUTH_CODE } from '../store/queries';
import FetchView from '../components/FetchView';
import Button from '../components/Button';
import Common from '../assets/Style';

const ButtonVerify = ({ validateId, authCode, setAuthState }) => {
    let verified = false;
    const [runQuery, { loading, error, data }] = useLazyQuery(VERIFY_SMS_AUTH_CODE);
    if (loading || error) return <FetchView loading={loading} error={error} />;
    if (data) {
        verified = data.verifySmsAuthCode.verified;
        if (verified) {
            setAuthState('done');
        }
    }
    return (
        <Button
            style={{
                ...Common.defaultButton,
                ...Common.roundedButton,
                borderWidth: 1,
                borderColor: '#7D14DE',
                width: 100,
                backgroundColor: verified ? '#7D14DE' : '#FFF'
            }}
            disabled={verified}
            onPress={() => runQuery({ variables: { id: validateId, code: authCode } })}
        >
            <Text style={{ color: verified ? '#FFF' : '#7D14DE' }} adjustsFontSizeToFit>
                {verified ? '인증완료' : '인증번호 확인'}
            </Text>
        </Button>
    );
};

export default ButtonVerify;
