import React, { useState } from 'react';
import { graphql } from '@apollo/react-hoc';
import { View, Text } from 'react-native';
import { useLazyQuery } from '@apollo/react-hooks';
import Timer from '../components/Timer';
import Button from '../components/Button';
import FetchView from '../components/FetchView';
import Common from '../assets/Style';

const ButtonAuth = ({ authState, setAuthState, setAuthCode }) => {
    return (
        <Button
            style={{
                ...Common.defaultButton,
                ...Common.roundedButton,
                borderWidth: 1,
                borderColor: '#7D14DE',
                backgroundColor:
                    authState === 'pending' || authState === 'done' ? '#fff' : '#7D14DE',
                width: 100
            }}
            disabled={authState !== 'pending'}
            onPress={() => setAuthState('request')}
        >
            {authState === 'pending' && (
                <Text style={{ color: '#7D14DE' }} adjustsFontSizeToFit>
                    인증번호 발송
                </Text>
            )}
            {authState !== 'pending' && (
                <Timer
                    authState={authState}
                    setAuthState={setAuthState}
                    setAuthCode={setAuthCode}
                />
            )}
        </Button>
    );
};

export default ButtonAuth;
