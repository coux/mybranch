import React, { useEffect } from 'react';
import { ViewStyle, View, Text } from 'react-native';
import { useMutation } from '@apollo/react-hooks';
import { useStore } from '../components/Hooks';
import { LOGOUT } from '../store/queries';
import EStyleSheet from 'react-native-extended-stylesheet';
import Button from './Button';
import Common from '../assets/Style';
const Header = props => {
    return (
        <View style={{ ...props.containerStyle, ...styles.container }}>
            <View style={styles.holder}>{props.children}</View>
            <Text style={{ ...props.titleStyle, ...styles.title }}>{props.title}</Text>
        </View>
    );
};

export default Header;

const styles = EStyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#F5F4F4',
        backgroundColor: '#fff'
    },
    holder: {
        flex: 1,
        justifyContent: 'center'
    },
    title: {
        position: 'absolute'
    }
});
