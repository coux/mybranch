import React, { useEffect } from 'react';
import { graphql } from '@apollo/react-hoc';
import { View, Text } from 'react-native';
import { UPDATED_TIMER } from '../store/queries';
import FetchView from '../components/FetchView';
import moment from 'moment';

const Timer = ({ authState, setAuthState, setAuthCode, data }) => {
    const { loading, error, timer } = data;
    console.log('loading, error, timer : ', loading, error, timer);
    if (loading || error) return <FetchView loading={loading} error={error} />;
    const { time, status } = timer;
    console.log('time: ', time);
    if (status === 'stoped') {
        setAuthState('pending');
        setAuthCode('');
    }
    return (
        <View>
            <Text style={{ color: authState === 'done' ? '#7D14DE' : '#fff' }}>{moment.utc(time).format('mm:ss')}</Text>
        </View>
    );
};

export default graphql(UPDATED_TIMER)(Timer);
