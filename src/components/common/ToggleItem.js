import React, { useEffect, useState } from 'react';
import { Text, TouchableOpacity } from 'react-native';

const ToggleItem = ({ style, item, selected, selectedColor, deselectedColor, onToggle }) => {
    return (
        <TouchableOpacity
            style={{ ...style, borderColor: selected ? selectedColor : deselectedColor }}
            onPress={() => {
                onToggle(!selected, item)
            }}
        >
            <Text style={{ color: selected ? selectedColor : deselectedColor }}>{item.value}</Text>
        </TouchableOpacity>
    );
};

export default ToggleItem;
