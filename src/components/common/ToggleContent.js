import React, { useState, useEffect, useCallback } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import ToggleItem from "./ToggleItem";

const ToggleContent = ({
    containerStyle,
    title,
    titleStyle,
    items,
    itemStyle,
    selecteds,
    onChange,
    limit,
}) => {
    const onToggle = useCallback(
        (toggle, item) => {
            toggle && (!limit || limit >= selecteds.length + 1)
                ? onChange([...selecteds, item])
                : onChange(selecteds.filter((el) => el.index !== item.index));
        },
        [selecteds]
    );
    return (
        <>
            <Text style={titleStyle}>{title}</Text>
            <View style={containerStyle}>
                {items.map((item, i) => (
                    <ToggleItem
                        style={itemStyle}
                        selectedColor={"#7D14DE"}
                        deselectedColor={"#C3C9D4"}
                        item={item}
                        selected={selecteds.findIndex((el) => el.index === item.index) > -1}
                        onToggle={onToggle}
                        key={`favor${i}`}
                    />
                ))}
            </View>
        </>
    );
};

export default ToggleContent;
