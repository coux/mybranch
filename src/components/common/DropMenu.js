import React, { Children } from 'react';
import { View, Text } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Button from '../Button';

const DropMenu = props => {
    const { title, containerStyle, contentStyle, children, open } = props;
    return (
        <View style={containerStyle}>
            <View style={styles.title}>
                <Text style={styles.titleText}>{title}</Text>
            </View>
            <View style={[contentStyle, styles.content]}>{children}</View>
            <View style={styles.footer}>
                <Button style={styles.button} onPress={() => open(false)}>
                    <Text style={[styles.buttonText, { color: '#C3C9D4' }]}>CANCEL</Text>
                </Button>
                <Button style={styles.button} onPress={() => open(false)}>
                    <Text style={[styles.buttonText, { color: '#7D14DE' }]}>OK</Text>
                </Button>
            </View>
        </View>
    );
};

export default DropMenu;

const styles = EStyleSheet.create({
    title: {
        backgroundColor: '#fff',
        height: '49rem',
        paddingHorizontal: 10,
        justifyContent: 'center',
        borderColor: '#C3C9D4',
        borderBottomWidth: 1
    },
    titleText: {
        fontSize: '16rem',
        color: '#000'
    },
    footer: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        backgroundColor: '#fff',
        borderColor: '#C3C9D4',
        borderTopWidth: 1,
        paddingRight: 20
    },
    button: {
        padding: 10
    },
    buttonText: {
        fontSize: 16
    }
});
