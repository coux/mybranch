import React, { useState, useEffect, useCallback } from 'react';
import { View, Text } from 'react-native';
import { useStore, useModal } from '../Hooks';
import Modal from 'react-native-modal';
import EStyleSheet from 'react-native-extended-stylesheet';
import Button from '../Button';

const Modals = props => {
    const { type, visible, children, open, ...rest } = props;
    return (
        <Modal
            {...rest}
            isVisible={visible}
            onSwipeComplete={() => open(false)}
            onBackdropPress={() => open(false)}
            onBackButtonPress={() => open(false)}
            style={[styles.container, styles[type]]}
        >
            {children}
        </Modal>
    );
};

export default Modals;

const styles = EStyleSheet.create({
    container: {
        position: 'absolute',
        width: '100%',
        height: '100%'
    },
    top: {
        justifyContent: 'flex-start',
        margin: 0
    },
    middle: {
        justifyContent: 'center',
        alignItems: 'center',
        margin: 0
    },
    bottom: {
        justifyContent: 'flex-end',
        margin: 0
    },
    rightModal: {
        margin: 0
    },

    resultModal: {
        justifyContent: 'flex-start',
        margin: 0
    }
});
