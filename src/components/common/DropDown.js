import React, { useState, useEffect, Children } from 'react';
import { View, Text } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Modals from './Modals';
import Button from '../Button';
import DropMenu from './DropMenu';
import ArrowDown from '../../assets/images/common/arrow-down-gray';

// import TextInput from "../InputText";
// import Common from "../../assets/Style";

const DropDown = props => {
    const [open, setOpen] = useState(false);
    const { title, placeholder, value, modal, menu, children } = props;
    return (
        <View style={[styles.container, props.containerStyle]}>
            {title && <Text style={[styles.title, props.titleStyle]}>{title}</Text>}
            <Button style={[styles.button, props.buttonStyle]} onPress={() => setOpen(true)}>
                <View style={[styles.btnTextWrap]}>
                    {Array.isArray(value) ? (
                        value.map((v, i) => {
                            return v ? (
                                <Text style={styles.btnValue} key={`desc${i}`}>
                                    {i > 0 && ' - '}
                                    {`${v.value}`}
                                </Text>
                            ) : (
                                i === 0 && (
                                    <Text style={styles.btnText} key={`desc${i}`}>
                                        {placeholder}
                                    </Text>
                                )
                            );
                        })
                    ) : (
                        <Text style={value ? styles.btnValue : styles.btnText}>
                            {value ? value.value : placeholder}
                        </Text>
                    )}
                </View>
                <ArrowDown width={24} height={24} />
            </Button>

            {children &&
                (modal ? (
                    /* 모달 옵션이 있을때 */
                    <Modals {...modal} visible={open} open={setOpen}>
                        <DropMenu {...menu} open={setOpen}>
                            {children}
                        </DropMenu>
                    </Modals>
                ) : (
                    /* 일반 드롭다운 일때 */
                    open && (
                        <DropMenu {...menu} open={setOpen}>
                            {children}
                        </DropMenu>
                    )
                ))}
        </View>
    );
};

export default DropDown;

const styles = EStyleSheet.create({
    container: {
        backgroundColor: '#fff'
    },
    title: {
        height: '15rem',
        marginBottom: 5,
        fontSize: 12,
        color: '#2A2E34'
    },
    button: {
        flex: 1,
        height: '49rem',
        alignSelf: 'stretch',
        borderWidth: 1,
        alignItems: 'center',
        flexDirection: 'row',
        borderColor: '#C3C9D4',
        borderRadius: 5,
        paddingHorizontal: 10,
        justifyContent: 'space-between'
    },
    btnTextWrap: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    btnText: {
        fontSize: '14rem',
        color: '#C3C9D4'
    },
    btnValue: {
        fontSize: '14rem',
        color: '#000'
    },
    bottomModal: {
        justifyContent: 'flex-end',
        margin: 0
    }
});
