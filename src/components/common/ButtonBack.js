import React, { ReactNode, FunctionComponent } from 'react';
import { ViewStyle, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import ArrowLeft from '../../assets/images/common/arrow-left';
import Button from '../Button';

export default props => {
    const { canGoBack, goBack } = useNavigation();
    return (
        canGoBack() && (
            <Button onPress={() => goBack()}>
                <ArrowLeft />
            </Button>
        )
    );
};
