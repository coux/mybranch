import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import Picker, { Item as PickerItem } from '@gregfrench/react-native-wheel-picker';

// import Button from '../Button';
// import { View, Text } from 'react-native';

const MenuPicker = props => {
    const { items, selected, onSelect, pickerStyle, pickerItemStyle } = props;

    return (
        <Picker
            style={pickerStyle}
            itemStyle={pickerItemStyle}
            selectedValue={selected}
            onValueChange={index => onSelect(index)}
        >
            {items
                .map(({ value }) => value)
                .map((value, i) => (
                    <PickerItem label={value} value={i} key={`{item}${i}`} />
                ))}
        </Picker>
    );
};

export default MenuPicker;

const styles = EStyleSheet.create({
    
});
