import React from 'react';
import { useAnimation } from './Hooks';
import { Animated, Easing } from 'react-native';
const AnimatedView = ({ children, animate, style, interpolate }) => {
    const animation = useAnimation({ animate, duration: 500 });
    const tween = {
        ...style,
        ...Object.keys(interpolate).reduce((acc, cur) => {
            acc[cur] = animation.interpolate({
                inputRange: [0, 1],
                outputRange: [0, interpolate[cur]]
            });
            return acc;
        }, {})
    };
    return <Animated.View style={tween}>{children}</Animated.View>;
};

export default AnimatedView;
