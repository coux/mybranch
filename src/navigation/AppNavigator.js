import React, { useEffect, useRef, useCallback, useState } from 'react';
import { View, Text, Image, StyleSheet, Platform, Alert } from 'react-native';
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import {
    navigationRef,
    routeRef,
    isMountedRef,
    getRouteName,
    defaultHeader,
    defaultTheme
} from './RefNavigator';
import { graphql } from '@apollo/react-hoc';
import { useMutation } from '@apollo/react-hooks';
import { useStore } from '../components/Hooks';
import { SESSION, SET_USER_LOCATION } from '../store/queries';
import { Transition } from 'react-native-reanimated';
import * as Permissions from 'expo-permissions';
import * as Location from 'expo-location';
import FetchView from '../components/FetchView';
import EStyleSheet from 'react-native-extended-stylesheet';

import Home from '../screens/Home';
import Join from '../screens/Join';
import Login from '../screens/Login';
// import Social from '../screens/Social';
// import Enter from '../screens/Enter';
// import Favor from '../screens/Favor';
// import Common from '../assets/Style';
import MyPhoto from '../screens/MyPhoto';
import MyDetail from '../screens/MyDetail';
// import Header from '../components/Header';
import MyFavor from '../screens/MyFavor';
import MyPage from '../screens/MyPage';
// 작업모음
import Etc from '../screens/Etc';
import Shop from '../screens/Shop';
import BuyItem from '../screens/BuyItem';
// 설정페이지
import Settings from '../screens/Settings';
// 사용자 디테일 페이지 (프로필 메인)
import UserDetail from "../screens/UserDetail";
// 쪽지페이지
import MyMessage from "../screens/MyMessage";
// 알림페이지
import MyNotice from "../screens/MyNotice";

/* 동적 헤더 타이틀 */
const getHeaderTitle = (route) => {
    // Access the tab navigator's state using `route.state`
    const routeName = route.state
        ? // Get the currently active route name in the tab navigator
          route.state.routes[route.state.index].name
        : // If state doesn't exist, we need to default to `screen` param if available, or the initial screen
          // In our case, it's "Feed" as that's the first screen inside the navigator
          route.params?.screen || 'Feed';

    switch (routeName) {
        case 'Feed':
            return 'News feed';
        case 'Profile':
            return 'My profile';
        case 'Account':
            return 'My account';
    }
};

const AppNavigator = ({ data: { me } }) => {
    const [store, updateStore] = useStore();
    const { Navigator, Screen } = createStackNavigator();

    useEffect(() => {
        // 만약 로그인 세션이 서버쪽에 있다면 스토어에 저장해 주자
        if (me) {
            updateStore({ me });
        }
    }, [me]);

    useEffect(() => {
        // 네이게이션 레퍼런스가 마운트 확인용
        isMountedRef.current = true;
        return () => (isMountedRef.current = false);
    }, []);

    //         // setLocation(location);

    return (
        <NavigationContainer
            ref={navigationRef}
            theme={defaultTheme}
            onStateChange={(state) => {
                const prev = routeRef.current;
                const current = getRouteName(state);
                if (prev !== current) {
                    updateStore({ routeName: current });
                }
                routeRef.current = getRouteName(state);
            }}
        >
            <Navigator screenOptions={defaultHeader}>
                <Screen name="Home" component={Home} />
                <Screen
                    name="Login"
                    component={Login}
                    options={{ ...defaultHeader, headerTitle: '로그인' }}
                />
                <Screen
                    name="Join"
                    component={Join}
                    options={{ ...defaultHeader, headerTitle: '회원가입' }}
                />
                <Screen
                    name="MyDetail"
                    component={MyDetail}
                    options={{ ...defaultHeader, headerTitle: '프로필수정' }}
                />
                <Screen
                    name="MyPhoto"
                    component={MyPhoto}
                    options={{ ...defaultHeader, headerTitle: '사진등록&수정' }}
                />
                <Screen
                    name="MyFavor"
                    component={MyFavor}
                    options={{ ...defaultHeader, headerTitle: '이상형설정' }}
                />
                <Screen
                    name="MyPage"
                    component={MyPage}
                    options={{ ...defaultHeader, headerTitle: '마이페이지' }}
                />
                <Screen
                    name="Etc"
                    component={Etc}
                    options={{ ...defaultHeader, headerTitle: '작업모음' }}
                />
                <Screen
                    name="Shop"
                    component={Shop}
                    options={{ ...defaultHeader, headerTitle: '상점' }}
                />
                <Screen
                    name="BuyItem"
                    component={BuyItem}
                    options={{ ...defaultHeader, headerTitle: '구매하기' }}
                />
                <Screen
                    name="Settings"
                    component={Settings}
                    options={{ ...defaultHeader, headerTitle: '설정' }}
                />
                <Screen
                    name="UserDetail"
                    component={UserDetail}
                    options={{ ...defaultHeader, headerTitle: '사용자 메인' }}
                />
                <Screen
                    name="MyMessage"
                    component={MyMessage}
                    options={{ ...defaultHeader, headerTitle: "쪽지" }}
                />
                <Screen
                    name="MyNotice"
                    component={MyNotice}
                    options={{ ...defaultHeader, headerTitle: "알림" }}
                />
            </Navigator>
        </NavigationContainer>
    );
};

export default graphql(SESSION)(AppNavigator);
