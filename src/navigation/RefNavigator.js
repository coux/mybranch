import React, { createRef } from 'react';
import { DefaultTheme } from '@react-navigation/native';
import User from '../components/User';
import Button from '../components/Button';
import ButtonBack from '../components/common/ButtonBack';
import Branch from '../assets/images/common/branch-text';

export const isMountedRef = createRef();
export const navigationRef = createRef();
export const routeRef = createRef();
export const setHeader = header => {
    navigationRef.current.setOption(header);
};

export const navigate = (name, params) => {
    if (isMountedRef.current && navigationRef.current) {
        navigationRef.current.navigate(name, params);
    }
};

/* 라우팅 트래킹 */
export const getRouteName = ({ routes, index }) => {
    const { state, name } = routes[index];
    if (state) {
        return getRouteName(state);
    }
    return name;
};

/* 래퍼 네비게이션의 기본 헤더 스타일 */
export const defaultHeader = {
    headerStyle: { backgroundColor: '#fff' },
    headerTitleStyle: {
        color: '#000'
    },
    headerBackTitleVisible: false,
    headerTitle: <Branch />,
    headerLeft: () => <ButtonBack />,
    headerRight: () => <User style={{ marginRight: 10 }} />
};

/* 테마 지정 */
export const defaultTheme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        background: '#fff'
    }
};
