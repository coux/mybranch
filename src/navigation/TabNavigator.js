import React, { useState, useEffect, useCallback, useLayoutEffect } from "react";
import SafeAreaView from "react-native-safe-area-view";
import { View, StyleSheet, TouchableOpacity } from "react-native";
import { navigationRef } from "./RefNavigator";
import { useStore } from "../components/Hooks";
import TabManin from "../assets/images/menu/ic_home";
import TabDate from "../assets/images/menu/ic_date";
import TabStore from "../assets/images/menu/ic_store";
import TabGroup from "../assets/images/menu/ic_group";
import TabUser from "../assets/images/menu/ic_mypage";
import AnimatedView from "../components/AnimatedView";

const menus = [
    {
        id: "main",
        categories: ["Home", "Join", "Login"],
        Icon: TabManin,
    },
    {
        id: "date",
        categories: ["MyFavor"],
        Icon: TabDate,
    },
    {
        id: "store",
        categories: ["MyPhoto"],
        Icon: TabStore,
    },
    {
        id: "group",
        categories: ["MyDetail"],
        Icon: TabGroup,
    },
    {
        id: "user",
        categories: ["MyPage"],
        Icon: TabUser,
    },
    {
        id: "etc",
        categories: ["Etc"],
        Icon: TabGroup,
    },
];

const TabNavigator = (props) => {
    const [menu, setMenu] = useState();
    const [store] = useStore();
    const { me, routeName } = store;
    useEffect(() => {
        if (routeName) {
            const menu = menus.find((menu) => menu.categories.find((cate) => cate === routeName));
            if (menu) {
                setMenu(menu);
            }
        }
    }, [routeName]);

    useEffect(() => {
        if (!me) {
            navigationRef.current.navigate("Home");
        }
    }, [me]);

    return (
        me && (
            <SafeAreaView style={[styles.container]}>
                {menus.map(({ id, Icon, categories }, i) => (
                    <TouchableOpacity
                        style={styles.tab}
                        key={`tab${i}`}
                        onPress={() => {
                            navigationRef.current.navigate(categories[0]);
                        }}
                    >
                        <Icon stroke={menu.id === id ? "#7D14DE" : "#C3C9D4"} />
                    </TouchableOpacity>
                ))}
            </SafeAreaView>
        )
    );
};

const styles = StyleSheet.create({
    container: {
        width: "100%",
        flexDirection: "row",
        justifyContent: "space-around",
        padding: 20,
        borderTopWidth: 1,
        borderTopColor: "#ebebeb",
        backgroundColor: "#fff",
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 8,
        shadowOpacity: 0.05,
        elevation: 4,
    },
    tab: {
        paddingHorizontal: 10,
    },
});

export default TabNavigator;
