import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect, useRef } from 'react';
import { ApolloProvider } from '@apollo/react-hooks';
import { Platform, Text, StyleSheet, View, Dimensions } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import client from './src/store';
import AppNavigator from './src/navigation/AppNavigator';
import TabNavigator from './src/navigation/TabNavigator';
import EStyleSheet from 'react-native-extended-stylesheet';
const { width } = Dimensions.get('window');

EStyleSheet.build({ $rem: width / 400 });
export default (props) => {
	return (
		<ApolloProvider client={client} addTypename={false}>
			<StatusBar style="auto" />
			<SafeAreaProvider>
				<AppNavigator />
            	<TabNavigator />
			</SafeAreaProvider>
	  </ApolloProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
