import React, { useState, useEffect, useRef } from 'react';
import { Platform, StatusBar, StyleSheet, View, Dimensions } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { SplashScreen, AppLoading } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import { ApolloProvider } from '@apollo/react-hooks';

import AppNavigator from './src/navigation/AppNavigator';
import TabNavigator from './src/navigation/TabNavigator';
import EStyleSheet from 'react-native-extended-stylesheet';

import useLinking from './src/navigation/LinkNavigator';
import client from './src/store';
import * as Font from 'expo-font';

const { width } = Dimensions.get('window');

EStyleSheet.build({ $rem: width / 400 });

export default ({ skipLoadingScreen }) => {
    // const [store, updateStore] = useStore();
    const [isLoadingComplete, setLoadingComplete] = useState(false);
    const [initialNavigationState, setInitialNavigationState] = useState();
    const containerRef = useRef();
    const { getInitialState } = useLinking(containerRef);

    useEffect(() => {
        (async () => {
            try {
                SplashScreen.preventAutoHide();
                setInitialNavigationState(await getInitialState());
                await Font.loadAsync({
                    ...Ionicons.font,
                    'space-mono': require('./src/assets/fonts/Montserrat-Bold.ttf'),
                    'montserrat-regular': require('./src/assets/fonts/Montserrat-Regular.ttf')
                });
            } catch (e) {
                console.warn(e);
            } finally {
                setLoadingComplete(true);
                SplashScreen.hide();
            }
        })();
    }, []);

    if (!isLoadingComplete && !skipLoadingScreen) {
        return <AppLoading />;
    } else {
        return (
            <ApolloProvider client={client} addTypename={false}>
                <SafeAreaProvider>
                    {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
                    <AppNavigator />
                    <TabNavigator />
                </SafeAreaProvider>
            </ApolloProvider>
        );
    }
};
